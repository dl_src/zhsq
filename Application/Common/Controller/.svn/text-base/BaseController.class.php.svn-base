<?php
namespace Common\Controller;
use Think\Controller;
use Think\Model;

/**
 * 项目基类
 * zhangxinhe 2015-12-25
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class BaseController extends Controller{

	protected function _initialize(){
		header("Content-type: text/html; charset=utf-8");
		define('JQ', 'http://apps.bdimg.com/libs/jquery/1.8.2/jquery.min.js');
		define('MAP', 'http://api.map.baidu.com/api?v=2.0&ak=7E10638907487f0b95529028323dc734');
		define('COM', '/Public/Common/');
		define('CSS', '/Public/' . MODULE_NAME . '/css/');
		define('JS', '/Public/' . MODULE_NAME . '/js/');
		define('IMG', '/Public/' . MODULE_NAME . '/images/');
	}

	/**
	 * 获取列表信息，支持分页
	 * @param string $field 需要查询的字段
	 * @param string $table 数据来源表
	 * @param string $where 查询条件
	 * @param string $order 排序规则
	 * @param string $page 是否分页
	 * @param array $join 表连接
	 * @param number $listrows 分页大小
	 * @param number $rollPage 每页页码数
	 *        zhangxinhe 2015-12-25
	 */
	protected function getList($field, $table, $where, $order, $page = false, $join = null, $listrows = 15, $rollPage = 10){
		$table = C('DB_PREFIX') . (is_array($table) ? implode(',' . C('DB_PREFIX'), $table) : $table);
		$model = new Model();
		if($page == true){
			$totalrows = $model->table($table)->where($where)->join($join)->count();
			$page = new \Think\Page($totalrows, $listrows, $rollPage);
			$limit = $page->firstRow . ',' . $page->listRows;
			$this->assign('page', $page->show());
		}
		return $model->table($table)->field($field)->join($join)->where($where)->order($order)->limit($limit)->select();
	}

	/**
	 * 获取记录信息
	 * @param string $field 需要查询的字段
	 * @param string $table 数据来源表
	 * @param string $where 查询条件
	 * @param string $join 表连接
	 *        zhangxinhe 2015-12-25
	 */
	protected function getInfo($field, $table, $where, $join = null){
		$table = C('DB_PREFIX') . (is_array($table) ? implode(',' . C('DB_PREFIX'), $table) : $table);
		return M()->table($table)->field($field)->join($join)->where($where)->find();
	}

	/**
	 * 插入和更新数据，更新时可能返回0，请用恒等判断
	 * @param array $data 数据源
	 * @param string $table 操作的数据表
	 * @param number $action 操作类型，1添加（默认），2更新
	 * @param string $where 更新条件
	 *        zhangxinhe 2015-12-25
	 */
	protected function updateData($data, $table, $action = 1, $where = ''){
		$model = M($table);
		if($model->create($data, $action)){
			if($action == 1){
				return $model->filter('trim')->add();
			}else{
				if(empty($where)){
					return $model->filter('trim')->save();
				}else{
					return $model->filter('trim')->where($where)->save();
				}
			}
		}
		return false;
	}

	/**
	 * 删除数据，返回值可能为0，请用恒等判断
	 * @param string $where 删除条件
	 * @param string $table 删除数据表
	 *        zhangxinhe 2015-12-25
	 */
	protected function deleteData($where, $table){
		return M($table)->where($where)->delete();
	}

	/**
	 * 返回处理结果
	 * @param boolean $result 处理结果
	 * @param array $info 提示消息
	 * @param string $url 跳转链接
	 * @param array $params 附带参数
	 *        zhangxinhe Dec 25, 2015
	 */
	protected function returnResult($result, $info = null, $url = null, $params = array()){
		$info = $info ? $info : array('操作成功', '操作失败');
		list($success, $error) = is_array($info) ? $info : array($info, $info);
		if($result === false){
			$data = array('info' => $error, 'status' => 0);
		}else{
			$data = array('info' => $success, 'status' => 1);
		}
		if($url !== false){
			$data['url'] = $url ? $url : U(CONTROLLER_NAME . '/index');
		}
		$this->ajaxReturn(array_merge($data, $params));
	}

	/**
	 * 获取小区列表
	 * zhangxinhe 2016年6月2日
	 */
	public function getAreaList(){
		return M('area')->field('id,name')->where('status=1 and id in (' . session('adminInfo.aids') . ')')->select();
	}

	/**
	 * 获取楼栋列表
	 * zhangxinhe 2016年6月2日
	 */
	public function getBlockList(){
		$aid = I('get.aid', 0, 'intval');
		if($aid > 0){
			return M('block')->field('id,name,units')->where(array('status' => 1, 'aid' => $aid))->order('sort desc')->select();
		}
		return false;
	}

	/**
	 * 获取房间列表
	 * zhangxinhe 2016年6月2日
	 */
	public function getRoomList(){
		$bid = I('get.bid', 0, 'intval');
		$uid = I('get.uid', 0, 'intval');
		if($bid > 0){
			$where = array('status' => 1, 'bid' => $bid);
			$uid > 0 && $where['uid'] = $uid;
			return M('room')->field('id,name')->where($where)->order('sort desc')->select();
		}
		return false;
	}

	/**
	 * 异步获取小区楼栋房号
	 * zhangxinhe 2016年6月3日
	 */
	public function ajaxGetABR(){
		$type = I('get.type', 0, 'intval');
		if($type == 1){
			$data = $this->getAreaList();
		}elseif($type == 2){
			$data = $this->getBlockList();
		}elseif($type == 3){
			$data = $this->getRoomList();
		}
		$this->ajaxReturn($data);
	}
}
?>