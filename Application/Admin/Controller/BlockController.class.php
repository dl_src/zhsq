<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class BlockController extends AdminController{

	public function index(){
		$where = 'b.aid=a.id and b.aid in (' . session('adminInfo.aids') . ')';
		$where .= I('get.aid', 0, 'intval') > 0 ? ' and  b.aid=' . I('get.aid', 0, 'intval') : '';
		$where .= I('get.name', '', 'strval') ? ' and b.name like "%' . I('get.name', '', 'strval') . '%"' : '';
		// $where .= I('get.status', -1, 'intval') == -1 ? '' : ' and b.status=' . I('get.status', -1, 'intval');
		if(I('get.status') < 2){
			// $where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and status='.I('get.status') : ' and status < 2';
			$where .= I('get.status') != '' && I('get.status') > -1 ? ' and b.status=' . I('get.status') : ' and b.status < 2';
		}
		$list = $this->getList('b.id,b.name,b.status,b.sort,b.units,a.name aname', array('block b', 'area a'), $where, 'sort desc', true);
		foreach($list as $k => $v){
			$v['units'] = str_replace('11', '无', $v['units']);
			$list[$k]['units'] = implode('单元,', array_filter(explode(',', $v['units']))) . '单元';
		}
		$this->assign('list', $list);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	}

	/**
	 * 添加楼栋
	 * yaoyingli 2015年12月29日
	 */
	public function add(){
		if(IS_POST){
			$_POST['units'] = '0,' . implode(',', I('post.units'));
			$result = $this->updateData($_POST, 'block');
			$this->returnResult($result);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 修改楼栋
	 * yaoyingli 2015年12月29日
	 */
	public function edit(){
		if(IS_POST){
			$_POST['units'] = '0,' . implode(',', I('post.units'));
			$result = $this->updateData($_POST, 'block', 2);
			$this->returnResult($result);
		}else{
			$info = $this->getInfo('id,aid,name,status,sort,units', 'block', 'id=' . I('get.id', 0, 'intval'));
			if($info){
				$info['units'] = explode(',', $info['units']);
				$this->assign('info', $info);
				$areaList = $this->getAreaList();
				$this->assign('areaList', $areaList);
				$this->display('add');
			}
		}
	}

	/**
	 * 删除楼栋
	 * yaoyingli 2015年12月29日
	 */
	public function del(){
		$id = I('get.id', 0, 'intval');
		$result = M('block')->where('id=' . $id)->setField('status', 2);
		// $result = $this->deleteData('id=' . I('get.id', 0, 'intval'), 'block');
		$this->returnResult($result);
	}
}