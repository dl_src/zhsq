<?php
namespace Wap\Controller;
use Common\Controller\WapController;
use Common;
/**
 * 投诉建议
 * huying Feb 20, 2016
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class ComplaintController extends WapController{
	/**
	 * 投诉建议
	 * 
	 */
	public function index(){
		$list = $this->getList('id,desc,times,status', 'complaint', 'oid='.session('fansInfo.typeid'), 'times desc',true);
		$this->assign('list', $list);
		$this->display();
	}
	/**
	 * 详细情况
	 * 
	 */
	public function detail(){
		M('owner_notice')->where('type = 5 and typeid='.I('get.id',0,'intval'))->setField('status', 0);
		$info = $this->getInfo('id,desc,pics,times,feedback,feedback_pic,deal_time,status,name','complaint', 'id='.I('get.id',0,'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		if(!empty($info['feedback_pic'])){
			$info['feedback_pic'] = explode(',', $info['feedback_pic']);
		}
		if($info['status'] == 2){
			$info['comment'] = $this->getInfo('c.times as ctimes,c.content,c.score','comment as c','type = 2 and orderid = ' . I('get.id',0,'intval'));
		}
		$this->assign('info', $info);
		$this->display();
	}
	/**
	 * 评论
	 * 
	 */
	public function comment(){
		$owner = $this->getInfo('bid,aid', 'owner_room', 'oid='.session('fansInfo.typeid'));
		$service = $this->getInfo('sid','complaint','id='.I('post.id', 0, 'intval'));
		//$service = $this->getInfo('s.id','service as s', 's.aid = '.$owner['aid'].' and s.bids like "%,'.$owner['bid'].',%"');
		$result = $this->updateData(array('oid' => session('fansInfo.typeid'),'rid' => session('fansInfo.rid'), 'times' => time(), 'type' =>2, 'typeid' =>$service['sid'], 'score' => I('post.score', 0, 'intval'), 'content' => $_POST['content'], 'status' => 1, 'aid' => session('fansInfo.aid'),'orderid'=>I('post.id', 0, 'intval')), 'comment');		
		if($result > 0){
			M('complaint')->where('id=' . I('post.id', 0, 'intval'))->setField('status', 2);
			//赠送积分
			$point = C('score_point');
			if($point > 0){
				\Common\Api\CommonApi::ownerPointAct(session('fansInfo.typeid'), $point,1, '投诉建议评价',owner);
				
			}
			$this->ajaxReturn(array('status' => 1, 'info' => '评价成功'));
		}
		$this->ajaxReturn(array('status' => -1, 'info' => '评价失败'));
	}
}