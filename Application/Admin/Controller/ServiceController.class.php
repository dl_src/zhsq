<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

/**
 * 客服专员
 * huying Dec 29, 2015
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class ServiceController extends AdminController{

	/**
	 * 列表
	 * huying Dec 29, 2015
	 */
	public function index(){
		$where = 'aid in(' . session('adminInfo.aids') . ')';
		$where .= I('get.aid') > 0 ? ' and aid=' . I('get.aid', 0, 'intval') : '';
		$where .= I('get.name') ? ' and name like "%' . I('get.name') . '%"' : '';
		$where .= I('get.area') ? ' and area like "%' . I('get.area') . '%"' : '';
		$where .= I('get.phone') ? ' and phone like "%' . I('get.phone') . '%"' : '';
		$where .= I('get.status', -1, 'intval') > -1 ? ' and status=' . I('get.status') : 'and status >-1 and status < 3';
		$list = $this->getList('id,name,phone,aid,status,fid,area', 'service', $where, 'id desc', true);
		$this->assign('list', $list);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	}

	/**
	 * 添加
	 * huying Dec 29, 2015
	 */
	public function add(){
		if(IS_POST){
			$id = M('service')->where(array('phone' => I('post.phone'), 'status' => array('lt', 3)))->getField('id');
			if($id > 0){
				$this->returnResult(false, '手机号已被使用');
			}
			$_POST['bids'] = implode(',', $_POST['bid']);
			$result = $this->updateData($_POST, 'service');
			$this->returnResult($result);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 修改
	 * huying Dec 29, 2015
	 */
	public function edit(){
		if(IS_POST){
			$_POST['bids'] = implode(',', $_POST['bid']);
			$result = $this->updateData($_POST, 'service', 2);
			$this->returnResult($result);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$info = $this->getInfo('id,name,aid,pic,desc,status,bids,phone', 'service', 'id=' . I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$this->display('add');
		}
	}

	/**
	 * 删除
	 * huying Dec 29, 2015
	 */
	public function del(){
		$result = M('service')->where('id=' . I('get.id', 0, 'intval'))->setField('status', -1);
		if($result !== false){
			//$this->updateData(array('type' => 0, 'typeid' => 0), 'wxfans', 2, 'type = 4 and typeid=' . I('get.id', 0, 'intval'));
			M('wxfans')->where('type=4 and typeid=' . I('get.id', 0, 'intval'))->save(array('typeid' => 0, 'type' => 0));
		}
		$this->returnResult($result);
	}

	/**
	 * 根据小区id找已分配的楼栋号
	 * huying Dec 29, 2015
	 */
	public function getServiceBlockByAid(){
		$where = 'status = 1 and aid=' . I('get.aid', 0, 'intval');
		$where .= $_GET['id'] > 0 ? ' and id !=' . I('get.id', 0, 'intval') : '';
		$list = $this->getList('bids', 'service', $where, 'id desc');
		$arr = ',';
		foreach($list as $k => $v){
			$arr .= $v['bids'] . ',';
		}
		$this->ajaxReturn($arr);
	}

	/**
	 * 审核客服
	 * huying Jan 19, 2016
	 */
	public function vette(){
		if($_POST['status'] == null){
			$this->ajaxReturn(array('status' => -1, 'info' => '请选择审核状态'));
		}
		if(empty($_POST['bid']) && $_POST['status'] == 1){
			$this->ajaxReturn(array('status' => -1, 'info' => '请选择管理的楼栋'));
		}
		$_POST['bids'] = ',' . implode(',', $_POST['bid']) . ',';
		$result = $this->updateData($_POST, 'service', 2);
		if($result !== false){
			$fid = $this->getInfo('name,phone,fid','service', 'id='. I('post.id', 0, 'intval'));
			if($fid > 0){
				$openid = M('wxfans')->where('id=' . $fid['fid'] . ' and status = 1')->getField('openid');
				if($openid){
					$tplMsgData = array('first' => array('value' => '你的申请已审核为'.$_POST['status'] == 1 ? '通过' : '不通过', 'colorm' => '#ff0000'), 'name'=> $fid['name'],'phone'=>$fid['phone'],'address'=> C('SITE_NAME'), 'remark' => array('value' => '感谢你使用' . C('SITE_NAME'), 'color' => '#173177'));
					$wechatAuth = \Common\Api\CommonApi::wechatAuthInfo();
					$result3 = $wechatAuth->sendTemplateMsg($openid, C('check_template'), '/Wap/Index/index', $tplMsgData);
				}
				$this->updateData(array('id' => $fid, 'type' => 4, 'typeid' => I('post.id', 0, 'intval')), 'wxfans', 2);
			}
		}
		$this->returnResult($result);
	}
}