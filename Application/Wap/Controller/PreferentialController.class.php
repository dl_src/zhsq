<?php
namespace Wap\Controller;
use Common\Controller\WapController;
use Common\Api\CommonApi;
use Common\Api\EmsApi;

/**
 * 特惠团
 * yaoyongli 2016年1月8日
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class PreferentialController extends WapController{
	protected function _initialize() {
		parent::_initialize ();
		//分享
	 	 if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
			$this->assign('shareJs', $shareJs);
		}else{
			if(session('fansInfo.type') != 1){
				$this->ajaxReturn(array('status' => -1, 'info' => '你没有权限'));
			}
		}  
	}
	/**
	 * 商品展示
	 * 
	 */
	public function index(){
		$list = $this->getList('id,name,present_price,sort,status,pics,category,credit,label', 'product', 'status=1 and num>0', 'sort desc', true);
		$datalist = array();
		$datalist2 = array();
		foreach ($list as $k => $v){
			if(!empty($v['label'])){
				$list[$k]['label'] = explode(' ', $v['label']);
			}	
			if(!empty($v['pics'])){
				$list[$k]['pics'] = explode(',', $v['pics']);
			}
			if($v['category'] == '0'){//积分购买的商品
				$datalist[count($datalist)] = $list[$k];
			}else{
				$datalist2[count($datalist2)] = $list[$k];
			}
		}
		$this->assign('list', $datalist);
		$this->assign("dislist",$datalist2);
		$this->display();
	}
	
	/**
	 * 商品详情
	 * 
	 */
	public function product_detail(){
		$info = $this->getInfo('id,name,present_price,original_price,num,limit_num,sort,desc,status,pics,category,credit', 'product', 'status=1 and id=' . I('get.id',0,'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		$this->assign('info', $info);
		$this->display();
	}
	 
	/*
	 * 添加订单
	 * 
	 */
	public function addOrder(){
		$productInfo = $this->getInfo('present_price,num,category,credit,status,name','product','id=' . I('get.id',0,'intval'));
		if($productInfo['num'] < I('get.num',0,'intval')){
			$this->ajaxReturn(array('status' => -1, 'info' => '库存不足'));
		}
		if($productInfo["status"]==0){
			$this->ajaxReturn(array('status' => -1, 'info' => '商品已下架'));
		}
		$_POST['single_time'] = time();
		$_POST['total'] = I('get.num',0,'intval');
		if($productInfo["category"] == 0){//钱付
			$_POST['order_amount'] = intval($_GET['num']) * $productInfo['present_price'];
			$_POST["prix"] = $productInfo['present_price'];
		}else{
			$_POST['order_amount'] = intval($_GET['num']) * $productInfo['credit'];
			$_POST["prix"] = $productInfo['credit'];
		}
		$_POST['oid'] = session('fansInfo.typeid');
		$_POST['area'] = session('fansInfo.area');
		$_POST['pid'] = I('get.id', 0, 'intval');
		$_POST['aid'] = session("fansInfo.aid");
		$_POST['style'] = $productInfo["category"];
		$_POST['owner'] = session('fansInfo.name');
		$_POST['phone'] = session('fansInfo.phone');
		$_POST['pname'] = $productInfo['name'];
		$_POST['rid'] = session('fansInfo.rid');
		$result = $this->updateData($_POST, 'product_order');
		if($result!=false){
			if($_POST['style'] == 0){//钱付
			$result2 = $this->updateData(array('money'=>$_POST['order_amount'],'create_time'=> time(),'status'=>1,'typeid'=>$result,'oid'=>session('fansInfo.typeid'), 'remark' => '特惠团支付','aid'=>session("fansInfo.aid"),'rid'=>session('fansInfo.rid'),'type'=>3),'payment');
		}
		/* else{//货到付
			$rssult2 = $this->updateData(array('money' => 0,'create_time'=>time(),'status'=>1,'type'=>3,'typeid'=>$result,'oid'=>session('fansInfo'),'rid'=>session('fansInfo.rid'), 'remark' => '特惠团支付','aid'=>session("fansInfo.aid")), 'payment');
		} */
		$info = $this->getInfo('o.name,o.phone,o.address,o.id,orm.area','owner o','o.id=' . session('fansInfo.typeid'),array('left join __OWNER_ROOM__ orm on orm.oid=o.id'));
		$_POST['name'] = $info['name'];
		$_POST['phone'] = $info['phone'];
		if(empty($info['address'])){
			$_POST['address']=session("fansInfo.address");
		}else{
			$_POST['address'] = $info['address'];
		}
		if($_POST){
			$_POST["orderid"] = $result;
			$result2 = $this->updateData($_POST, 'product_logistics');
		}
		M('product')->where('id='.I('get.id',0,'intval'))->setDec('num',I('get.num',0,'intval'));
		$this->ajaxReturn(array('status' => 1, 'id' => $result, 'info' => '操作成功'));
		}else{
			$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
		}
	}
	
	/*
	 * 订单详情
	 * 
	 */
	public function order_detail(){
		$info = $this->getInfo('p.name as title,p.status as product,p.pics,s.prix,s.style,s.pid,s.oid,s.order_amount,s.single_time,s.pay_type,s.pay_amount,s.total,s.id,s.status',array('product p','product_order s'),'s.pid= p.id and s.id='.I('get.id',0,'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		$infoman =$this->getInfo('name,phone,address','product_logistics','orderid='.I('get.id',0,'intval'));
		$this->assign('infoman',$infoman);
		$this->assign('info',$info);
		$this->display();
	}
	
	/**
	 * 支付方式
	 * 
	 */
	public function pay_type(){
		$payment = $this->getInfo('id,status,money','payment','type=3 and typeid=' . I('get.id',0,'intval'));
		$orderInfo = $this->getInfo('order_amount,status,pid,total','product_order','id=' . I('get.id',0,'intval'));
		$pid = $orderInfo['pid'];
		if($orderInfo['status'] == 0){
		if($_GET['type'] ==3){ //积分
			$maninfo = $this->getInfo('id,point', 'owner', 'id=' . session('fansInfo.typeid'));
			if($maninfo['point'] < $orderInfo['order_amount']){
				$this->ajaxReturn(array('status' => -1, 'info' => '积分不足'));
			}else{
			$point = $orderinfo['order_amount'];
			\Common\Api\CommonApi::ownerPointAct(session('fansInfo.typeid'),$point,'特惠团积分支付',0,session('fansInfo.name'));
		}
		$data = array('pay_type'=>3,'status'=>2,'pay_amount'=>$orderInfo['order_amount'],'pay_time'=> time(),'id'=>I('get.id',0,'intval'));
	}else{
		$data = array('pay_type' => 2, 'status' => 2, 'pay_amount' => 0, 'pay_time' => time(), 'id' => I('get.id', 0, 'intval'));
	}
	$result1 = $this->updateData(array('id' => $payment['id'], 'pay_type' => 2, 'status' => 2, 'real_money' => $payment['money'], 'pay_time' => time()), 'payment', 2);
	$result = $this->updateData($data, 'product_order', 2);
	if($result !==false){
		if($_GET['type'] != 3){
			$point = floor($payment['money'] / C('get_money') * C('get_point'));
			\Common\Api\CommonApi::ownerPointAct(session('fansInfo.typeid'), $point,'特惠团积分赠送',1,user);
		}
		$this->ajaxReturn(array('status' => 2, 'info' => '操作成功'));
	}else{
		$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
	}
		}
	}
	/**
	 * 购买的商品评价
	 * 
	 */
	public function comment(){
		$info = $this->getInfo('pid,pay_amount', 'product_order', 'id=' . I('post.id', 0, 'intval'));
		if(!empty($info)){
			$result = $this->updateData(array('id' => I('post.id', 0, 'intval'), 'status' => 5), 'product_order', 2);
			if($result > 0){
				$result1 = $this->updateData(array('oid' => session('fansInfo.typeid'),'times' => time(), 'type' => 3, 'typeid' => $info['pid'],'orderid'=>I('post.id', 0, 'intval'), 'score' => I('post.score', 0, 'intval'), 'content' => $_POST['content'], 'rid' => session('fansInfo.rid'), 'status' => 1,'aid'=>session('fansInfo.aid')), 'comment');
				if($result1 > 0){
					$point = C('score_point');
					if($point > 0){
						//$this->changePoint(session('fansInfo.oid'), $point, '特惠团评价', 4, I('post.id', 0, 'intval'));
						\Common\Api\CommonApi::ownerPointAct(session('fansInfo.typeid'), $point, '特惠团评价',1,user);
					}
					$this->ajaxReturn(array('status' => 5, 'info' => '评价成功'));
				}
			}
		}
		$this->ajaxReturn(array('status' => -1, 'info' => '评价失败'));
	}
	
	/**
	 * 判断是否评价
	 * 
	 */
	public function evaluate(){
		$info = $this->getInfo('l.oid,l.name,l.phone,l.address,s.pid,s.id,s.status,s.order_amount,s.pay_amount,s.single_time,s.pname,s.prix,p.pics,s.total', array('product_logistics l','product p','product_order s'), 'p.status=1 and s.pid=p.id and s.oid=l.oid and s.id=' . I('get.id', 0, 'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		if($info['status'] == 5){//已经评论
			$info['comment'] = $this->getInfo('times,content,score', 'comment', 'type = 3 and orderid = '.I('get.id', 0, 'intval'));
		}
		$this->assign('info', $info);
		$this->display();
	}
	
	/**
	 * 获取订单id
	 * 
	 */
	public function getPid(){
		$pid = M('payment')->where('type = 3 and typeid=' . I('post.id', 0, 'intval'))->getField('id');
		$this->ajaxReturn($pid);
	}
	
	/**
	 * 确认收货方式
	 * 
	 */
	public function typea(){
		$payment = $this->getInfo('id,status,money', 'payment', 'type =3 and typeid=' . I('get.id',0,'intval'));
		$orderinfo = $this->getInfo('order_amount,status', 'product_order', 'id=' . I('get.id',0,'intval'));
		if($orderinfo['status'] == 3){
			$result1 = $this->updateData(array('id' => $payment['id'], 'real_money' => $payment['money'], 'pay_time' => time()), 'payment', 2);
			$data = array('status' => 4, 'pay_amount' => $orderinfo['order_amount'], 'id' => I('get.id', 0, 'intval'));
			$result = $this->updateData($data, 'product_order', 2);
			if($result !== false){
				$this->ajaxReturn(array('status' => 4, 'info' => '操作成功'));
			}else{
				$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
			}
		}else{
			$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
		}
	}
	
	/**
	 * 特惠团我的订单页面逻辑删除,status为6
	 * 
	 */
	public function del(){
		$statusinfo = $this->getInfo('status', 'product_order', 'id=' . I('get.id', 0, 'intval'));
		if($statusinfo['status'] == 1 or $statusinfo['status'] == 5){
			$data = array('status' => 6, 'id' => I('get.id', 0, 'intval'));
			$result = $this->updateData($data, 'product_order', 2);
			if($result !== false){
				$this->ajaxReturn(array('status' => 6, 'info' => '操作成功'));
			}else{
				$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
			}	
		}
	}
	
	/**
	 * 特惠团我的订单
	 * 
	 */
	public function orders(){
		$_POST['single_time'] = time();
		$list = $this->getList('p.name,s.prix,s.style,p.sort,p.status as pstatus,p.pics,s.total,s.pid,s.oid,s.status,s.single_time,s.order_amount,s.id as id',array('product p','product_order s'), 's.pid=p.id and s.status<>6 and s.oid='.session('fansInfo.typeid'), 'single_time desc',true);
		foreach($list as $k => $v){
			if(!empty($v['pics'])){
				$list[$k]['pics'] = explode(',', $v['pics']);
			}
		}
		$this->assign('list', $list);
		$this->display();
	}
	
	/**
	 * 特惠团物流信息页面
	 *
	 */
	public function logistics(){
		$info = $this->getInfo('id,name,phone,address,ems_name,ems_num', 'product_logistics', 'orderid=' . I('get.id', 0, 'intval'));
		$this->assign('info', $info);
		$list = \Common\Api\EmsApi::findEms(\Common\Api\EmsApi::searchExpress($info['ems_name']), $info['ems_num']);
		$this->assign('list', $list);
		$this->display();
	}
	
	/*
	 * 特惠团取消订单
	 * 
	 */
	public function status(){
		$statusinfo = $this->getInfo('status,pid,total', 'product_order', 'id=' . I('get.id', 0, 'intval'));
		if($statusinfo['status'] == 0){
			$data = array('pay_type' => 0, 'status' => 1, 'id' => I('get.id', 0, 'intval'));
			$result = $this->updateData($data, 'product_order', 2);
			if($result !== false){
				M('product')->where('id=' . $statusinfo['pid'])->setInc('num', $statusinfo['total']);
				$this->ajaxReturn(array('status' => 1, 'info' => '操作成功'));
			}else{
				$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
			}
		}else{
			$this->ajaxReturn(array('status' => -1, 'info' => '操作失败,你已下单,请刷新!'));
		}
	}
	
// 	public function status(){
// 		$info = $this->getInfo('s.id,s.status,s.total,p.name,s.pid',array('product p','product_order s'),'s.pid=p.id and s.id='. I('get.id',0,'intval') and  s.status<>6 . ' and s.oid='.session('fansInfo.typeid'));
// 	    if($info['status'] == 0){
// 	    	$data = array('pay_type' => 0, 'status' => 1, 'id' => I('get.id',0,'intval'));
// 	    	$result = $this->updateData($data,'product_order',2);
// 	    	/* if($result !== false){
// 	    		M('product')->where('id=' . $info['pid'])->setInc('n');
// 	    	} */
// 	    }
// 	}
}