<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

/**
 * 商家列表
 * huying Dec 23, 2015
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class MerchantController extends AdminController{

	public function index(){
		$where = 'm.aid = a.id and m.type_id = t.id and m.aid in(0,' . session('adminInfo.aids') . ')';
		$where .= I('get.type_id') > 0 ? ' and m.type_id=' . I('get.type_id', 0, 'intval') : '';
		$where .= I('get.aid') > 0 ? ' and m.aid=' . I('get.aid', 0, 'intval') : '';
		$where .= I('get.name') ? ' and m.name like "%' . I('get.name') . '%"' : '';
		// $where .= I('get.status', -1, 'intval') == -1 ? '' : ' and m.status=' . I('get.status', -1, 'intval');
		if(I('get.status') < 2){
			// $where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and status='.I('get.status') : ' and status < 2';
			// $where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and o.status ='.I('get.status') : ' and o.status < 2';
			$where .= I('get.status') != '' && I('get.status') > -1 ? ' and m.status=' . I('get.status') : ' and m.status < 2';
		}
		$list = $this->getList('m.id,m.name,m.type_id,t.name as type,m.sort,m.status,m.phone,a.name as area', array('merchant m', 'merchant_type as t', 'area a'), $where, 'm.id desc', true);
		$this->assign('list', $list);
		$typeList = $this->getList('id,name', 'merchant_type', 'status = 1', 'id desc');
		$this->assign('typeList', $typeList);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	}

	public function add(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'merchant');
			$this->returnResult($result);
		}else{
			$typeList = $this->getList('id,name', 'merchant_type', 'status = 1', 'id desc');
			$this->assign('typeList', $typeList);
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'merchant', 2);
			$this->returnResult($result);
		}else{
			$typeList = $this->getList('id,name', 'merchant_type', 'status = 1', 'id desc');
			$this->assign('typeList', $typeList);
			$info = $this->getInfo('id,aid,address,phone,name,mapx,mapy,type_id,pic,desc,sort,status', 'merchant', 'id=' . I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display('add');
		}
	}

	public function del(){
		/*
		 * $result = $this->deleteData('id=' . I('get.id', 0, 'intval'), 'merchant');
		 * $this->returnResult($result);
		 */
		$result = M('merchant')->where('id=' . I('get.id', 0, 'intval'))->setField('status', 2);
		$this->returnResult($result);
	}
}