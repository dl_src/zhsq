<?php
    if(C('LAYOUT_ON')) {
        echo '{__NOLAYOUT__}';
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>页面跳转中</title>
<style>p{text-align:center;}</style>
</head>
<body>
<?php if(isset($message)) {?>
<p><?php echo($message); ?></p>
<?php }else{?>
<p><?php echo($error); ?></p>
<?php }?>
<p>页面将于<b id="wait"><?php echo($waitSecond); ?></b>秒后自动 <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a></p>
<script type="text/javascript">
(function(){
var wait = document.getElementById('wait'),href = document.getElementById('href').href;
var interval = setInterval(function(){
	var time = --wait.innerHTML;
	if(time <= 0) {
		location.href = href;
		clearInterval(interval);
	};
}, 1000);
})();
</script>
</body>
</html>
