<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class RoomController extends AdminController{

	public function index(){
		$where = 'r.aid=a.id and r.bid=b.id and r.aid in (' . session('adminInfo.aids') . ')';
		$where .= I('get.aid', 0, 'intval') > 0 ? ' and r.aid=' . I('get.aid', 0, 'intval') : '';
		$where .= I('get.bid', 0, 'intval') > 0 ? ' and r.bid=' . I('get.bid', 0, 'intval') : '';
		$where .= I('get.uid', 0, 'intval') > 0 ? ' and r.uid=' . I('get.uid', 0, 'intval') : '';
		$where .= I('get.name', '', 'strval') ? ' and r.name like "%' . I('get.name', '', 'strval') . '%"' : '';
		$where .= I('get.status', -1, 'intval') == -1 ? '' : ' and r.status=' . I('get.status', -1, 'intval');
		$list = $this->getList('r.id,r.uid,r.name,r.size,r.status,r.sort,r.owner,r.phone,a.name area,b.name block', array('room r', 'block b', 'area a'), $where, 'r.sort desc', true);
		$this->assign('list', $list);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	}

	/**
	 * 添加房号
	 * yaoyingli 2015年12月29日
	 */
	public function add(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'room');
			$this->returnResult($result);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 修改房号
	 * yaoyingli 2015年12月29日
	 */
	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'room', 2);
			$result1 = M('block')->where('id=' . $_POST['bid'])->setField('rid', $_POST['id']);
			if($result1 > 0 && $_POST['old_bid'] != $_POST['rid']){
				M('block')->where('id=' . $_POST['rid_rid'])->setField('rid', 0);
			}
			$this->returnResult($result);
		}else{
			$info = $this->getInfo('id,owner,phone,aid,bid,uid,name,size,status', 'room', 'id=' . I('get.id', 0, 'intval'));
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->assign('info', $info);
			$this->display('add');
		}
	}

	/**
	 * 删除房号
	 * yaoyingli 2015年12月29日
	 */
	public function del(){
		$result = M('room')->where('id=' . I('get.id', 0, 'intval'))->setField('status', 2);
		// $result = $this->deleteData('id=' . $_GET['id'], 'room');
		$this->returnResult($result);
	}

/**
 * 根据单元号获取房间名
 * huying Dec 28, 2015
 */
	/*
	 * public function getRoomByUid(){
	 * $list = $this->getList('id,name,oid', 'room', 'bid = ' . I('get.bid', 0, 'intval') . ' and uid = ' . I('get.uid', 0, 'intval'), 'sort desc,id asc');
	 * $this->ajaxReturn($list);
	 * }
	 */
}