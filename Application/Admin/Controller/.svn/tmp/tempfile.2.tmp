<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

/**
 * 业主管理
 * huying Dec 28, 2015
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class OwnerController extends AdminController{

	/**
	 * 业主列表
	 * huying Dec 28, 2015
	 */
	public function index(){
		$where = 'orm.aid in (' . session('adminInfo.aids') . ')';
		$where .= I('get.aid', 0, 'intval') > 0 ? ' and orm.aid=' . I('get.aid', 0, 'intval') : '';
		$where .= I('get.bid', 0, 'intval') > 0 ? ' and orm.bid=' . I('get.bid', 0, 'intval') : '';
		$where .= I('get.uid', 0, 'intval') > 0 ? ' and orm.uid=' . I('get.uid', 0, 'intval') : '';
		$where .= I('get.name') ? ' and o.name like "%' . I('get.name') . '%"' : '';
		$where .= I('get.phone') ? ' and o.phone like "%' . I('get.phone') . '%"' : '';
		$where .= I('get.start_time') ? ' and o.reg_time>' . strtotime(I('get.start_time')) : '';
		$where .= I('get.end_time') ? ' and o.reg_time<' . (strtotime(I('get.end_time')) + 24 * 3600) : '';
		$where .= I('get.status', -1, 'intval') > -1 ? ' and o.status=' . I('get.status', -1, 'intval') : ' and o.status < 2';
		if(I('get.act') == 1){
			$list = $this->getList('o.name,o.phone,orm.area,orm.room,o.point,o.nickname,from_unixtime(o.reg_time),o.status', 'owner_room orm', $where, 'o.id desc', true, array('left join __OWNER__ o on orm.oid=o.id'));
			$title = array('业主', '手机', '小区', '房号', '积分', '昵称', '注册时间', '状态（0表示未审核，1表示已审核）');
			array_unshift($list, $title);
			\Common\Api\PHPExcelApi::exportExcel($list, 'Owner_' . date('Ymd'));
		}else{
			$list = $this->getList('o.id,o.pid,o.fid,o.name,o.phone,o.point,o.nickname,o.reg_time,o.status,orm.area,orm.room', 'owner_room orm', $where, 'o.id desc', true, array('left join __OWNER__ o on orm.oid=o.id'));
			\Think\Log::write(M()->_sql(), 'zhsqss');
			$this->assign('list', $list);
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 添加业主
	 * huying Dec 28, 2015
	 */
	public function add(){
		if(IS_POST){
			if(!(I('post.aid') > 0 && I('post.bid') > 0 && I('post.uid') > 0 && I('post.rid'))){
				$this->returnResult(false, '数据不完整');
			}
			$id = M('owner')->where(array('phone' => I('post.phone'), 'status' => array('lt', 2)))->getField('id');
			if($id > 0){
				$this->returnResult(false, '手机号已被使用');
			}
			$_POST['reg_time'] = time();
			$result = $this->updateData($_POST, 'owner');
			if($result){
				$_POST['oid'] = $result;
				$this->updateData($_POST, 'owner_room');
				\Common\Api\CommonApi::ownerPointAct($result, C('point_regedit'), '注册送积分', 1, session('adminInfo.name'));
			}
			$this->returnResult($result);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 获取业主积分记录
	 * huying Mar 3, 2016
	 */
	public function getPoint(){
		if(IS_POST){
			$result = \Common\Api\CommonApi::ownerPointAct(I('get.oid', 0, 'intval'), I('post.point', 0, 'intval'), I('post.name'), I('post.type'), session('adminInfo.name'));
			if($result === 0){
				$this->returnResult(false, '积分余额不足');
			}
			$this->returnResult($result, null, U('Owner/getPoint', $_GET));
		}
		$where = 'oid=' . I('get.oid');
		$where .= I('get.start_time') ? ' and times>=' . strtotime(I('get.start_time')) : '';
		$where .= I('get.end_time') ? ' and times<' . (strtotime(I('get.end_time')) + 24 * 3600) : '';
		$where .= I('get.type', 0, 'intval') > 0 ? ' and type = ' . I('get.type', 0, 'intval') : '';
		$list = $this->getList('id,point,times,name,type,users', 'point', $where, 'times desc', true);
		$this->assign('list', $list);
		$this->display();
	}

	/**
	 * 删除业主
	 * huying Dec 28, 2015
	 */
	public function del(){
		$id = I('get.id', 0, 'intval');
		$result = M('owner')->where('id=' . $id)->setField('status', 2);
		if($result){
			$this->deleteData(array('oid' => $id), 'owner_room');
			M('wxfans')->where('type=1 and typeid=' . $id)->save(array('typeid' => 0, 'type' => 0));
		}
		$this->returnResult($result);
	}

	/**
	 * 业主信息
	 * huying Jan 13, 2016
	 */
	public function detail(){
		$info = $this->getInfo('o.name,o.phone,orm.area,orm.room,r.owner,r.phone rphone', 'owner o', 'o.id=' . I('post.id', 0, 'intval'), array('left join __OWNER_ROOM__ orm on orm.oid=o.id', 'left join __ROOM__ r on orm.rid=r.id'));
		$this->returnResult($info, null, null, $info);
	}
	/**
	 * 审核
	 * huying Jan 13, 2016
	 */
	public function check(){
		$id = I('post.id', 0, 'intval');
		$type = I('post.type', 1, 'intval');
		$result = M('owner')->where(array('id' => $id))->setField('status', ($type == 1 ? 1 : 2));
		if($result){
			$owner = $this->getInfo('name,phone,address,fid', 'owner', 'id='.$id);
			$openid = M()->table('__OWNER__ o,__WXFANS__ f')->where('o.fid=f.id and o.id=' . $id)->getField('f.openid');
			if($openid){
				$tplMsgData = array('first' => array('value' => '你的申请已审核为'.$_POST['status'] == 1 ? '通过' : '不通过', 'colorm' => '#ff0000'), 'name'=> $owner['name'],'phone'=>$owner['phone'],'adress'=>C('SITE_NAME'), 'remark' => array('value' => '感谢你使用' . C('SITE_NAME'), 'color' => '#173177'));
				$wechatAuth = \Common\Api\CommonApi::wechatAuthInfo();
				$wechatAuth->sendTemplateMsg($openid, C('check_template'), '/Wap/Index/index', $tplMsgData);
			}
			if(I('post.type', 0, 'intval') == 1){
				$this->updateData(array('id'=> $owner['fid'],'type' => 1, 'typeid' => $id), 'wxfans', 2);
			}
		}
		$this->returnResult($result);
	}
}