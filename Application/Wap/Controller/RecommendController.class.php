<?php
namespace Wap\Controller;
use Common\Controller\WapController;
/**
 * 星房推荐
 * huying Mar 10, 2016
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class RecommendController extends WapController{
	protected function _initialize() {
		parent::_initialize ();
		//分享
		 if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
			$this->assign('shareJs', $shareJs);
		} 
	}
	/**
	 * 首页
	 * yaoyongli 2016年1月7日
	 */
	public function index(){
		$where = 'status = 1';
		$where .= session('fansInfo.aid') > 1 ? ' and aid='.session('fansInfo.aid') : '';
		$list = $this->getList('id,name,aid,pics', 'recommend', $where, 'sort desc',true);
		foreach ($list as $k => $v){
//			if(!empty($v['pics'])){
//				$list[$k]['pics'] = explode(',', $v['pics']);
//			}
// 			$list[$k]['area'] = $v['aid'] > 0 ? M('area')->where('id='.$v['aid'])->getField('name') : '全部小区';
			$list[$k]['area'] = M('area')->where('id='.$v['aid'])->getField('name');
		}

		$this->assign('list', $list);
		$this->display();
	}
	/**
	 * 详细情况
	 * huying Mar 10, 2016
	 */
	public function detail(){
		$info = $this->getInfo('r.id,r.name,r.desc,r.pics,r.phone,r.aid,a.name as area', array('recommend r','area a'), 'r.aid = a.id and r.id='.I('get.id', 0, 'intval'));
		$info['pics'] = explode(',', $info['pics']);
// 		var_dump($info);exit;
		$this->assign('info', $info);
		$this->display();
	}
}