<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class BookingTypeController extends AdminController{

	public function index(){
		$where = '1=1';
		$where .= I('get.name', '', 'strval') ? ' and name like "%' . I('get.name', '', 'strval') . '%"' : '';
		// $where .= I('get.status', -1, 'intval') == -1 ? '' : ' and status=' . I('get.status', -1, 'intval');
		// $where .= I('get.status', -1, 'intval') < 2 ? '' : ' and status=' . I('get.status', -1, 'intval');
		if(I('get.status') < 2){
			$where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and status=' . I('get.status') : ' and status < 2';
			// $where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and o.status ='.I('get.status') : ' and o.status < 2';
			// $where .= I('get.status') != '' && I('get.status') > -1 ? ' and status='.I('get.status') : ' and status < 2';
		}
		$list = $this->getList('id,name,desc,sort,status', 'booking_Type', $where, 'id desc', true);
		$this->assign('list', $list);
		$this->display();
	}

	public function add(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'booking_type');
			$this->returnResult($result);
		}else{
			$this->display();
		}
	}

	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'booking_type', 2);
			$this->returnResult($result);
		}else{
			$info = $this->getInfo('id,name,desc,sort,status', 'booking_type', 'id=' . I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$this->display('add');
		}
	}

	/*
	 * public function setStatus(){
	 * $status = I('get.status', 0, 'intval');
	 * $result = M('booking_type')->where('id=' . I('get.id', 0, 'intval'))->setField('status',$status );
	 * $this->returnResult($result);
	 * }
	 */
	public function del(){
		$id = I('get.id', 0, 'intval');
		$result = M('booking_type')->where('id=' . $id)->setField('status', 2);
		$this->returnResult($result);
	}
}