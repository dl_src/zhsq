<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

/**
 * 后台管理
 * huying Dec 23, 2015
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class IndexController extends AdminController{

	public function index(){
		$this->display();
	}
}