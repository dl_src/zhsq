<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class GroupCommentController extends AdminController{

	public function index(){
		$_POST['times'] = time();
		$where = 'c.type = 3 and c.typeid = go.id and c.aid in(' . session('adminInfo.aids') . ')';
		$list = $this->getList('c.id,go.owner,c.score,c.times,c.type,c.typeid,c.rid,go.phone,c.status,go.pname,go.pay_time', array('comment c', 'product_order go'), $where, 'times desc', true);
		$this->assign('list', $list);
		$this->display();
	}

	public function detail(){
		if(IS_AJAX){
			$info = $this->getInfo('id,pid,owner,phone,pname,pay_time,status', 'product_order s', 'id=' . I('get.id', 0, 'intval'));
			if($info['status'] == 5){
				$info['comment'] = $this->getInfo('times,desc,score', 'comment', 'type = 3 and typeid = ' . I('get.id', 0, 'intval'));
			}
			if(!empty($info['pics'])){
				$info['pics'] = explode(',', $info['pics']);
			}
			$this->assign('info', $info);
			// 订单跟踪
			$list = $this->getList('id,rid,name,phone,content,times', 'follow', 'type = 2 and typeid=' . I('get.id', 0, 'intval'), 'times asc');
			$this->assign('list', $list);
			$this->display();
		}
	}
}