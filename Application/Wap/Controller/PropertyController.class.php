<?php
namespace Wap\Controller;
use Common\Controller\WapController;
/**
 * 通知公告
 * yaoyongli 2016年1月4日
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class PropertyController extends WapController{
	protected function _initialize() {
		parent::_initialize ();
		//分享
		if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
			$this->assign('shareJs', $shareJs);
		} 
	}
	
	public function index(){
		$info = $this->getInfo('area,room','owner_room','oid='.session('fansInfo.typeid'));
		$this->assign('info',$info);

		//获取幻灯片
		$where = 'status = 1';
		//$where .= session('fansInfo.type') == 1 ? ' and aid like "%,'.session('fansInfo.aid').',%"' : '';
		$slideList = $this->getList('id,pic,url', 'slide', $where, 'id desc',true);
		$this->assign('slideList', $slideList);
		
		$this->display();
	}
	/**
	 * 列表
	 * huying Jan 13, 2016
	 */
	public function notice(){
		$list = $this->getList("id,desc,times,title,status", "notice", '(aid=0 or aid='.session('fansInfo.aid').') and status=1', "times desc", true);	
		$this->assign('list', $list);
		$this->display();
	}
	/**
	 * 删除
	 * huying Jan 13, 2016
	 */
	public function del(){
		$result = M('notice')->where('id in ('.$_POST['ids'].')')->setField('status', 2);
		$this->returnResult($result);
	}
	/**
	 * 系统发布的通知
	 * huying Jan 22, 2016
	 */
 	public function notice_details(){
		if(IS_POST){
			$result = M('owner_notice')->where('type = 1 and typeid='.I('post.id', 0, 'intval').' and oid='.session('fansInfo.typeid'))->setField('status', 0);
			if($result > 0 && I('post.status') == 1){
				M('notice')->where('id='.I('post.id', 0, 'intval'))->setInc('look');
			}
		}else{
			$info = $this->getInfo('id,status,title,desc,times', 'notice', 'id='.I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$this->display();
		}
	} 
}