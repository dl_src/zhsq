<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class GroupProductController extends AdminController{

	public function index(){
		$where = '1=1';
		$where .= I('get.name', '', 'strval') ? ' and name like "%' . I('get.name', '', 'strval') . '%"' : '';
		$where .= I('get.category', -1, 'intval') == -1 ? '' : ' and category=' . I('get.category', -1, 'intval');
		if(I('get.status') < 2){
			$where .= I('get.status') != '' && I('get.status') > -1 ? ' and status=' . I('get.status') : ' and status < 2';
		}
		$list = $this->getList('id,name,num,present_price,original_price,sort,category,status,credit', 'product', $where, 'sort desc', true);
		$this->assign('list', $list);
		$this->display();
	}

	public function add(){
		if(IS_POST){
			$_POST['pics'] = implode(',', $_POST['pic']);
			$_POST['limit_num'] = $_POST['num'];
			$result = $this->updateData($_POST, 'product');
			$this->returnResult($result);
		}else{
			$this->display();
		}
	}

	public function edit(){
		if(IS_POST){
			$_POST['pics'] = implode(',', $_POST['pic']);
			$_POST['limit_num'] = $_POST['num'];
			$result = $this->updateData($_POST, 'product', 2);
			$this->returnResult($result);
		}else{
			$info = $this->getInfo('id,name,num,present_price,original_price,sort,status,pics,desc,category,credit,label', 'product', 'id=' . I('get.id', 0, 'intval'));
			$info['pics'] = explode(',', $info['pics']);
			$this->assign('info', $info);
			$this->display('add');
		}
	}

	public function setStatus(){
		$status = I('get.status', 0, 'intval');
		$result = M('product')->where('id=' . I('get.id', 0, 'intval'))->setField('status', $status);
		$this->returnResult($result);
	}
	
	public function setSort(){
		$sort = I('get.sort', 100, 'intval');
		$result = M('product')->where('id=' . I('get.id', 0, 'intval'))->setField('sort', $sort);
		$this->returnResult($result);
	}

	public function del(){
		$id = I('get.id', 0, 'intval');
		$result = M('product')->where('id=' . $id)->setField('status', 2);
		$this->returnResult($result);
	}
}