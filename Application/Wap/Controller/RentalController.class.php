<?php
namespace Wap\Controller;
use Common\Controller\WapController;

/**
 * 租售服务
 * yaoyongli 2016年1月8日
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class RentalController extends WapController{
	protected function _initialize() {
		parent::_initialize ();
		//分享
		 if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
			$this->assign('shareJs', $shareJs);
		}else{
			if(session('fansInfo.type') != 1 && ACTION_NAME != 'want'){
				$this->ajaxReturn(array('status' => -1, 'info' => '你没有权限'));
			}
		} 
	}
	/**
	 * 首页,也是出租列表页
	 *
	 */
	public function index(){
		$where='(s.type=1 or s.type=3 or s.type=5) and s.status=1 and s.aid=a.id';
		$where .= I('get.type', '0', 'intval') == 0 ? '' : ' and s.type=' . I('get.type', 0, 'intval');
		$where .= I('get.area', 0, 'intval') > 0 ? ' and a.id='.I('get.area', 0, 'intval') : '';
		$order='s.times desc';
		if($_GET['sort']==1){
			$order='s.price+0 asc,s.times desc';
			}else if($_GET['sort'] == 2){
					$order='s.price+0 desc,s.times desc';
				}else{
					$order='s.times desc';
				}
				$list=$this->getList('s.id,s.title,s.status,s.pics,s.type,s.size,s.price,s.aid,s.times,s.area',array('rental_service as s','area as a'),$where, $order,true);
				$a=M()->getLastSql();
				foreach($list as $k => $v){
					if(!empty($v['pics'])){
						$list[$k]['pics'] = explode(',', $v['pics']);
					}
				}
				$this->assign('list',$list);
				//var_dump($list);exit;
				$areaList = $this->getList('id,name', 'area', 'status = 1', 'id asc');
				$this->assign('areaList', $areaList);
				//$shareJs = $this->getShareJs('出租出售列表业', C('share_pic'), U('Rental/index'), '出租出售列表业');
				//$this->assign('shareJs', $shareJs);
				$this->display();
	}
	
	/**
	 * 出售列表页
	 */
	public function sell_index(){
		$where='(s.type=2 or s.type=4) and s.status=1 and s.aid=a.id';
		$where .= I('get.type', '0', 'intval') == 0 ? '' : ' and s.type=' . I('get.type', 0, 'intval');
		$where .= I('get.area', 0, 'intval') > 0 ? ' and a.id='.I('get.area', 0, 'intval') : '';
		$order='s.times desc';
		if($_GET['sort']==1){
			$order='s.price+0 asc,s.times desc';
		}else{
			$order='s.price+0 desc,s.times desc';
		}
		$list=$this->getList('s.id as id,s.title,s.status,s.pics,s.type,s.size,s.price,s.times,s.area,s.aid',array('rental_service as s','area as a'),$where, $order,true);
		foreach($list as $k => $v){
			if(!empty($v['pics'])){
				$list[$k]['pics'] = explode(',', $v['pics']);
			}
		}
		$this->assign('list',$list);
		//var_dump($list);exit;
		$areaList = $this->getList('id,name', 'area', 'status = 1', 'id asc');
		$this->assign('areaList', $areaList);
		$this->display();
	}
	
	/**
	 * 租售服务的列表详情页
	 */
	public function rent_detail(){
		$info=$this->getInfo('s.oid,s.id as id,s.title,s.status,s.pics,s.size,s.price,s.aid,s.times,s.room,s.type,s.hall,s.toilet,s.size,s.floor_several,s.address,s.desc,s.floor,s.area,s.phone','rental_service s', 's.id='.I('get.id', 0, 'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		$this->assign('info',$info);
		$this->display();
	}
	
	/**
	 * 租售服务的出售
	 */
	public function sell(){
		if(IS_POST){
			$_POST['times'] = time();
			$_POST['type'] = $_GET['type'];
			$_POST['pics'] = implode(',', $_POST['pic']);
			$_POST['oid'] = session('fansInfo.typeid');
			$_POST['rid'] = session('fansInfo.rid');
			$_POST['owner'] = session('fansInfo.name');
			$_POST['phone'] = session('fansInfo.phone');
			$_POST['status']=1;
			$style = I('post.id', 0, intval) > 0 ? 2 : 1;
			$result = $this->updateData($_POST, 'rental_service', $style);
			$this->returnResult($result);
		}else{
			if(I('get.id',0,intval) >　0){
			$info=$this->getInfo('id,type,size,desc,price,room,hall,toilet,floor_several,floor,aid,address,pics,title,house', 'rental_service', 'id='.I('get.id',0,intval));
			if(!empty($info['pics'])){
				$info['pics'] = array_filter(explode(',', $_POST['pics']));
			}
			$info['count'] = count($info['pics']);
			$this->assign('info',$info);
			$infoman=$this->getInfo('id,name,phone', 'owner', 'id='.session('fansInfo.typeid'));
			$this->assign('infoman',$infoman);
			}
			$areaList = $this->getList('id,name', 'area', 'status = 1', 'id asc');
			$this->assign('areaList', $areaList);
			
			$data=array_merge(array("scope"=>'temp',"deadline"=>time()+3600),array('returnUrl'=>C('site_url').U('Wap/Rental/upload',array('fid'=>session('fansInfo.fid'))),'returnBody'=>'{"url":"http://7xjfxt.com1.z0.glb.clouddn.com/$(key)"}'));
			$token=$this->token($data, 'XAX8AcIUuCVrjqOx3t6ZF9U3gHaIG9fVvc6xQSl1', 'JZ6bJQR2nt4cjJBONXqaUnBp64DXTExN4iJtyPZk');
			$this->assign('token',$token);
			$this->display();
		}
		// $this->display();
	}
	
	/**
	 * 租售服务的用户提交信息页面
	 */
	public function want(){

			$_POST['submit_time']=time();
			$_POST['status']=0;
			$_POST["type"]=$_POST['type'];
			$_POST['phone'] = $_POST['phone'];
			$_POST['oid'] = session('fansInfo.typeid');
			$_POST['rid'] = session('fansInfo.rid');
			// 		存在反馈表的类型
			$info=$this->getInfo('id,phone', 'rental_service_intention', 'sid='.$_POST["sid"]." and phone = '".$_POST['phone']."'");
			if(!$info){
				$result=$this->updateData($_POST, 'rental_service_intention');
				$this->returnResult($result);
				if($result !== false){
					$this->ajaxReturn(array('status' => 0, 'info' => '操作成功'));
				}else{
					$this->ajaxReturn(array('status' => -1, 'info' => '操作失败'));
				}
			}else{
				$this->ajaxReturn(array('status' => -1, 'info' => '已提交意向'));
			}

	}
	/**
	 * 租售服务的出租
	 */
	public function lease(){
		if(IS_POST){
			$pics = explode(',', $_POST['pics']);
			foreach ($pics as $key => $value) {
				$wechatAuth = \Common\Api\CommonApi::wechatAuthInfo();
				$access_token = F('access_token');
				$url = 'http://file.api.weixin.qq.com/cgi-bin/media/get?access_token='.$access_token['access_token'].'&media_id='.$value;
				$list[] = qiniuFetch($url);
			}
			$_POST['pics'] = implode(',', $list);
			$_POST['times']=time();
			$_POST['oid'] = session('fansInfo.typeid');
			$_POST['type']=$_GET['type'];
			$_POST['status']=1;
			$style = I('post.id', 0, intval) > 0 ? 2 : 1;
			$result = $this->updateData($_POST, 'rental_service', $style);
			$this->returnResult($result);
		}else{
			if(I('get.id',0,intval) >　0){
				$info=$this->getInfo('id,type,size,desc,price,room,hall,toilet,floor_several,floor,aid,address,pics,title,house', 'rental_service', 'id='.I('get.id',0,intval));
				if(!empty($info['pics'])){
					$info['pics'] = explode(',', $_POST['pics']);
				}
				$info['count'] = count($info['pics']);
				$this->assign('info',$info);
			}
			$areaList = $this->getList('id,name', 'area', 'status = 1', 'id asc');
			$this->assign('areaList', $areaList);
			/*
			$data=array_merge(array("scope"=>'temp',"deadline"=>time()+3600),array('returnUrl'=>C('site_url').U('Wap/Rental/upload',array('fid'=>session('fansInfo.fid'))),'returnBody'=>'{"url":"http://7xjfxt.com1.z0.glb.clouddn.com/$(key)"}'));
			$token=$this->token($data, 'XAX8AcIUuCVrjqOx3t6ZF9U3gHaIG9fVvc6xQSl1', 'JZ6bJQR2nt4cjJBONXqaUnBp64DXTExN4iJtyPZk');
			$this->assign('token',$token);*/
			$this->display();
		}
	}
	/**
	 * 删除租售服务
	 */
	public function del(){
// 		$result = $this->deleteData('id=' . $_GET['id'], 'rental_service');
        $POST['status']=0;
        $result =$this->updateData($POST, 'rental_service',2,'id=' . $_GET['id']);
		$this->returnResult($result);
	}
	
	/**
	 * 租售服务的我的页面
	 */
 	public function mine(){
		$list=$this->getList('id,type,title,times,status,desc','rental_service', 'status =1 and oid='.session('fansInfo.typeid'),'times desc',true);
		$this->assign('list',$list);
		$this->display();
	}
	
	/**
	 *七牛云上传Token生成 
	 */
	private function token($data,$accessKey,$secretKey){
		$data=str_replace(array('+','/'), array('-','_'), base64_encode(json_encode($data)));
		$lease=hash_hmac('sha1', $data, $secretKey,true);
		return $accessKey.':'.str_replace(array('+','/'), array('-','_'), base64_encode($lease)).':'.$data;
	}
	
	/**
	 * 上传照片
	 */
	public function upload(){
		$str = json_decode(base64_decode(str_replace(array('-', '_'), array('+', '/'), $_GET['upload_ret'])), true);
		exit('{"error":0, "url": "'.$str['url'].'"}');
	}
}