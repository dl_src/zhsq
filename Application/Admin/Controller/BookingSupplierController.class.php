<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class BookingSupplierController extends AdminController{

	public function index(){
		$where = 's.tid=t.id';
		$where .= I('get.id', 0, 'intval') > 0 ? ' and t.id=' . I('get.id', 0, 'intval') : '';
		$where .= I('get.typeid') > 0 ? ' and t.id=' . I('get.typeid', 'intval') : '';
		$where .= I('get.name', '', 'strval') ? ' and s.name like "%' . I('get.name', '', 'strval') . '%"' : '';
		$where .= I('get.status', -1, 'intval') == -1 ? '' : ' and s.status=' . I('get.status', -1, 'intval');
		$list = $this->getList('s.id,s.tid,s.name,s.sort,s.phone,s.address,s.desc,s.status,s.pic,t.name as type', array('booking_supplier s', 'booking_type t'), $where, 'sort asc', true);
		$this->assign('list', $list);
		$typeList = $this->getList('id ,name', 'booking_type', 'status =1');
		$this->assign('typeList', $typeList);
		$this->display();
	}

	public function add(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'booking_supplier');
			$this->returnResult($result);
		}else{
			$typeList = $this->getList('id,name as type', 'booking_type', 'status=1', 'id desc');
			$this->assign('typeList', $typeList);
			$this->display();
		}
	}

	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'booking_supplier', 2);
			$this->returnResult($result);
		}else{
			$typeList = $this->getList('id,name as type', 'booking_type', 'status=1', 'id desc');
			$this->assign('typeList', $typeList);
			$info = $this->getInfo('id,tid,name,phone,address,desc,service,price,sort,status,pic', 'booking_supplier', 'id=' . I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$this->display('add');
		}
	}

	public function del(){
		$result = $this->deleteData('id=' . I('get.id', 0, 'intval'), 'booking_supplier');
		$this->returnResult($result);
		// $info=$this->getInfo('', 'booking_supplier', 'id=' . $_GET['id']);
		$result1 = $this->deleteData('rid=' . I('get.id', 0, 'intval'), 'comment');
		$this->returnResult($result1);
	}
}