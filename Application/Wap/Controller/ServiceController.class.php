<?php
namespace Wap\Controller;
use Common\Controller\WapController;
/**
 * 客服
 * huying Jan 19, 2016
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class ServiceController extends WapController{
	private $manInfo;
	protected function _initialize() {
		parent::_initialize ();
// 		获取客服的信息
		$this->manInfo = $this->getInfo('s.id,s.name,s.pic,s.phone,a.name as area,s.bids,s.score', array('service as s', 'area as a'), 's.aid = a.id and s.id='.session('fansInfo.typeid'));
		$this->assign('manInfo', $this->manInfo);
		 if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
			$this->assign('shareJs', $shareJs);
		} 
	}
	
	/**
	 * 客服首页
	 * huying Jan 19, 2016
	 */
	public function index(){
		$bids = trim($this->manInfo['bids'], ',');
		switch(I('get.type', 0, 'intval')){
			case 1://缴费通知
				$list = $this->getList('id,name,desc,times', 'service_notice', 'type = 1 and sid='.session('fansInfo.typeid'), 'times desc', true);
				break;
			case 2://投诉建议
				$bids = trim($this->manInfo['bids'], ',');
				
				$list = $this->getList('id,desc,times,status','complaint', 'bid in ('.$bids.') and status<3 and sid = '.session('fansInfo.typeid'),'times desc', true);
					
				break;
			case 3://预约通知
				$list = $this->getList('b.id,s.name,s.desc,s.times,s.status as nstatus,b.status', array('service_notice s','booking b'), 's.type = 2 and b.id = s.typeid and s.sid='.session('fansInfo.typeid'), 'times desc', true);
				break;
			case 4://评论
				$list = $this->getlist('c.typeid,c.content,c.score','comment as c','c.type = 2 and c.typeid ='. session('fansInfo.typeid'),'c.times desc',true);
				break;
		}
		$this->assign('list', $list);
		$this->display();
	}
	/**
	 * 投诉详情
	 * huying Jan 21, 2016
	 */
	public function complaint(){
		$info = $this->getInfo('c.name,c.phone,c.feedback,c.feedback_pic,c.deal_time,c.desc,c.times,c.pics,c.status,s.name as sname',array('complaint as c','service as s'), 'c.id='.I('get.id', 0, 'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		if(!empty($info['feedback_pic'])){
			$info['feedback_pic'] = explode(',', $info['feedback_pic']);
		}
		/* if($info['status'] == 2){
			$info['comment'] = $this->getInfo('times,content,score', 'comment', 'type = 2 and typeid = '.I('get.id', 0, 'intval'));
		} */
		$this->assign('info', $info);
		$this->display(); 
	}
	
	/**
	 * 投诉反馈
	 * huying Jan 21, 2016
	 */
	public function feedback(){
		$info = $this->getInfo('id,oid', 'complaint', 'id='.I('post.id', 0, 'intval'));
		\Think\Log::write(M()->_sql(), 'own');
		if($_POST['pics']){
			$pics = array_filter(explode(',', $_POST['pics']));
			$wechatAuth = \Common\Api\CommonApi::wechatAuthInfo();
			foreach ($pics as $value) {
				$feedback_pic[] = $wechatAuth->getFileToQiniu($value);
			}
		}
		$result = $this->updateData(array('id' => I('post.id', 0, 'intval'), 'feedback' => $_POST['feedback'], 'feedback_pic' => implode(',', $feedback_pic), 'deal_time' =>time(), 'status' => 1), 'complaint', 2);
		\Think\Log::write(M()->_sql(), 'cccccc');
		if($result){
			M('warn')->where('type = 3 and typeid='.I('post.id', 0, 'intval').' and status = 1')->setField('status', 0);
			$result2 = \Common\Api\CommonApi::addNotice($info['oid'], $_POST['feedback'], '你的投诉/建议已受理', 5, $info['id']);
			$openid = M('wxfans')->where('type=1 and typeid=' . $info['oid'])->getField('openid');
			\Think\Log::write(M()->_sql(), 'owner');
			if($openid){
				$data = array('first' => array('value' => '您的投诉建议已处理完成！', 'color' => '#ff0000'), 'keyword1' => array('value' => session('fansInfo.name'), 'color' => '#173177'), 'keyword2' => array('value' => session('fansInfo.phone'), 'color' => '#173177'),
						'keyword3' => array('value' => date('Y-m-d H:i', time()), 'color' => '#173177'), 'keyword4' => array('value' => '已受理', 'color' => '#173177'), 'keyword5' => array('value' => session('fansInfo.name'), 'color' => '#173177'),'remark' => array('value' => '点击查看详情', 'color' => '#173177'));
				$wechatAuth = \Common\Api\CommonApi::wechatAuthInfo();
				$wechatAuth->sendTemplateMsg($openid, C('advise_return_template'), U('Complaint/detail?id=' . $info['id']), $data);
			}
		}
		$this->returnResult($result);
	}

	
	/**
	 * 预约详情
	 *
	 */
	public function booking(){
		if(IS_POST){
			if(I('post.id', 0, 'intval') > 0){
				$result = M('booking')->where('id='.I('post.id', 0, 'intval'))->setField('status', 3);
				if($result !== false){
					M('warn')->where('type = 2 and typeid='.I('post.id', 0, 'intval').' and status = 1')->setField('status', 0);
					M('service_notice')->where('type = 2 and typeid = '.I('post.id', 0, 'intval'))->setField('status', 0);
					$owner = $this->getInfo('b.oid',array('booking as b','owner as o'), 'b.oid = o.id and b.id='.I('post.id', 0, 'intval'));
					//添加通知消息
					$typeid = \Common\Api\CommonApi::addNotice($owner['oid'], '预约新进度', '你的预约已经处理，请及时查看', 6, I('post.id', 0, 'intval'));
					$this->ajaxReturn(array('status' => 1, 'info' => '操作成功'));
				}
			}
			$this->ajaxReturn(array('status' => 0, 'info' => '操作失败'));
		}else{
			$info = $this->getInfo('b.id,b.status,b.day,b.hour,b.name,b.phone,b.desc,b.submit_time,t.name as catename,s.name as company,s.phone as com_phone',array('booking b','booking_type t','booking_supplier s'),'b.tid = t.id and b.sid = s.id and b.id='.I('get.id', 0, 'intval'));
			/* if($info['status'] == 2){
				$info['comment'] = $this->getInfo('times,desc,score', 'comment', 'type = 4 and rid = '.I('get.id', 0, 'intval'));
			} */
			$this->assign('info', $info);
			$this->display();
		}
	}

	/**
	 * 上传头像
	 * huying Mar 7, 2016
	 */
	public function upload(){
		$result = M('service')->where('id='.session('fansInfo.typeid'))->setField('pic', $_POST['pic'][0]);
		$this->returnResult($result);
	}
	
	/**
	 * 我的评价
	 * 
	 */
	public function comment(){
		$info = $this->getInfo('c.name,c.phone,c.feedback,c.feedback_pic,c.desc,c.pics,c.status,com.score,com.content,com.times',array('complaint as c','comment as com'),'com.type = 2 and c.id = com.orderid and c.sid='.I('get.id', 0, 'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		if(!empty($info['feedback_pic'])){
			$info['feedback_pic'] = explode(',', $info['feedback_pic']);
		}
		$this->assign('info', $info);
		$this->display();
		
	}
}