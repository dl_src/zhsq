<?php
namespace Common\Api;
/**
 * 公共通用接口
 * zhangxinhe 2015-12-25
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class CommonApi{

	/**
	 * 初始化微信高级接口
	 * zhangxinhe 2015-12-25
	 */
 	public static function wechatAuthInfo(){
		$access_token = F('access_token');

		if($access_token['expire_in'] < time()){
			$wechatAuth = new WechatAuthApi(C('site_appid'), C('site_appsecret'));
			$access_token = $wechatAuth->getAccessToken();
			F('access_token', array('access_token' => $access_token, 'expire_in' => time() + 7000));
			return $wechatAuth;
		}else{
			return new WechatAuthApi(C('site_appid'), C('site_appsecret'), $access_token['access_token']);
		}
	}
 
	/**
	 * 业主积分操作
	 * @param integer $oid 业主ID
	 * @param integer $point 积分数
	 * @param string $desc 操作原因
	 * @param integer $type 类型 1 增加（默认） 2减少
	 * @param string $users 操作员
	 *        zhangxinhe 2016年6月22日
	 */
	public static function ownerPointAct($oid, $point, $desc, $type = 1, $users = null){
		if($oid > 0 && $point > 0){
			if($type == 2){
				$now_point = M('owner')->where(array('id' => $oid))->getField('point');
				if($now_point < $point){
					return 0;
				}
			}
			$result = M('owner')->where(array('id' => $oid))->setInc('point', $type == 1 ? $point : -1 * $point);
			if($result){
				M('point')->add(array('oid' => $oid, 'point' => $point, 'name' => $desc, 'type' => $type, 'users' => $users, 'times' => time()));
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 添加业主的通知
	 * @param intger $oid 身份的id
	 * @param string $title 标题
	 * @param string $desc 说明
	 * @param intger $type 类型（1：系统公告；2：报修进度处理，3：亲属/租客申请，4：订单处理通知,5:投诉建议受理通知,6:预约处理）
	 * @param intger $typeid 类型id
	 * huying Jan 13, 2016
	 */
	public static function addNotice($oid, $title, $desc, $type, $typeid){
		return M('owner_notice')->add(array('oid' => $oid, 'title' => $title, 'desc' => $desc, 'type' => $type, 'typeid' => $typeid, 'times' => time()));
	}

	/**
	 * 添加信息跟踪
	 * @param intger $oid
	 * @param string $oname
	 * @param string $otel
	 * @param number $type 报修： 1 ，特惠团： 2 ，租房售房： 3 ，预约： 4, 投诉建议 ：5
	 * @param intger $typeid
	 * @param string $content
	 * huying Dec 30, 2015
	 */
	public static function addFollow($oid, $oname, $otel, $type, $typeid, $content){
		return M('follow')->add(array('typeid' => $oid, 'name' => $oname, 'phone' => $otel, 'type' => $type, 'cate_id' => $typeid, 'times' => time(), 'content' => $content));
	}

	/**
	 * 阿里大鱼短信发送接口
	 * @param string $phone 手机号 支持多个手机号，最多不超过200个，多个号码时使用数组传入
	 * @param string $sign_name 短信签名 必须为审核通过的签名
	 * @param string $template 短信模板 必须为审核通过的模板
	 * @param array $parsm 附加参数，不同模板参数不同
	 * @return boolean zhangxinhe 2016年5月31日
	 */
	public static function sendSms($phone, $sign_name, $template, $parsm = null){
		$paramsData = array('method' => 'alibaba.aliqin.fc.sms.num.send', 'app_key' => '23365060', 'timestamp' => date('Y-m-d H:i:s'), 'format' => 'json', 'v' => '2.0', 'sign_method' => 'md5');
		$postData = array('sms_type' => 'normal', 'sms_free_sign_name' => $sign_name, 'rec_num' => $phone, 'sms_template_code' => $template, 'sms_param' => json_encode($parsm));
		$data = array_merge($paramsData, $postData);
		ksort($data);
		foreach($data as $k => $v){
			$str .= $k . $v;
		}
		$paramsData['sign'] = strtoupper(md5('70410f17fc3d6ec7743b3ac620e9df71' . $str . '70410f17fc3d6ec7743b3ac620e9df71'));
		$result = json_decode(http('http://gw.api.taobao.com/router/rest', $paramsData, $postData, 'POST'), true);
		return $result['alibaba_aliqin_fc_sms_num_send_response']['result']['err_code'] == 0 ? true : false;
	}

	/**
	 * 邮件发送
	 * @param string $user 收件人邮箱
	 * @param string $content 邮件内容
	 * @param string $subject 邮件主题
	 *        zhangxinhe 2015-12-25
	 */
	public static function sendEmail($user, $content, $subject){
		vendor('PHPMailer.class#phpmailer');
		$mail = new \PHPMailer();
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->CharSet = "UTF-8";
		$mail->Host = 'smtp.mxhichina.com';
		$mail->Port = '25';
		$mail->Username = 'service@weilt.net';
		$mail->Password = 'ahdl3880882';
		$mail->From = 'service@weilt.net';
		$mail->Subject = $subject;
		$mail->FromName = "微老头客户服务部";
		$mail->AddAddress($user, "尊敬的微老头用户");
		$mail->IsHTML(true);
		$mail->Body = $content;
		return $mail->Send();
	}
}