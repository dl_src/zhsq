<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

/**
 * 维修员管理
 * 
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class RepairmanController extends AdminController{

	/**
	 * 列表
	 * 
	 */
	public function index(){
		$where = 'aid in (' . session('adminInfo.aids') . ')';
		$where .= I('get.aid', 0, 'intval') > 0 ? ' and aid=' . I('get.aid', 0, 'intval') : '';
		$where .= I('get.name') ? ' and name like "%' . I('get.name') . '%"' : '';
		$where .= I('get.phone') ? ' and phone like "%' . I('get.phone') . '%"' : '';
		$where .= I('get.status', -1, 'intval') > -1 ? ' and status=' . I('get.status', -1, 'intval') : 'and status >-1 and status < 3';
		$list = $this->getList('id,name,phone,area,status,fid', 'repairman', $where, 'id desc', true);
		$this->assign('list', $list);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	}

	/**
	 * 添加
	 * 
	 */
	public function add(){
		if(IS_POST){
			$id = M('repairman')->where(array('phone' => I('post.phone'), 'status' => array('lt', 3)))->getField('id');
			if($id > 0){
				$this->returnResult(false, '手机号已被使用');
			}
			$result = $this->updateData($_POST, 'repairman');
			$this->returnResult($result);
			var_dump($_POST);exit;
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 修改
	 * 
	 */
	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'repairman', 2);
			$this->returnResult($result);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$info = $this->getInfo('id,name,phone,aid,area,pic,desc,status', 'repairman', 'id=' . I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$this->display('add');
		}
	}

	/**
	 * 删除
	 * 
	 */
	public function del(){
		$result = M('repairman')->where('id=' . I('get.id', 0, 'intval'))->setField('status', -1);
		if($result){
			//$this->updateData(array('type' => 0, 'typeid' => 0), 'wxfans', 2, 'type=3 and typeid=' . I('get.id', 0, 'intval'));
			M('wxfans')->where('type=3 and typeid=' . I('get.id', 0, 'intval'))->save(array('typeid' => 0, 'type' => 0));
		}
		$this->returnResult($result);
	}

	/**
	 * 审核维修工
	 * zhangxinhe 2016年6月7日
	 */
	public function checked(){
		$result = M('repairman')->where(array('id' => I('get.id', 0, 'intval')))->setField('status', 1);
		\Think\Log::write(M()->_sql(), 'zxh1');
		if($result){
			$fid = M('repairman')->where('id=' . I('get.id', 0, 'intval'))->getField('fid');
			\Think\Log::write(M()->_sql(), 'zxh2');
			$openid = M('wxfans')->where(array('status' => 1, 'typeid' => I('get.id', 0, 'intval')))->getField('openid');
			if($openid){
				$tplMsgData = array('first' => array('value' => '您的申请已审核通过！', 'color' => '#ff0000'), 'keyword1' => array('value' => '维修员入驻', 'color' => '#173177'), 'keyword2' => array('value' => '审核通过', 'color' => '#173177'), 'keyword3' => array('value' => '无', 'color' => '#173177'), 
					'remark' => array('value' => '感谢您使用微老头智慧社区系统', 'color' => '#173177'));
				$wechatAuth = \Common\Api\CommonApi::wechatAuthInfo();
				$result3 = $wechatAuth->sendTemplateMsg($openid, C('check_template'), '/Wap/Index/index', $tplMsgData);
			}
			$this->updateData(array('id' => $fid, 'type' => 3, 'typeid' => I('get.id', 0, 'intval')), 'wxfans', 2);
			\Think\Log::write(M()->_sql(), 'zxh0');
		}
		$this->returnResult($result);
	}
}