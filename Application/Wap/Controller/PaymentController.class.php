<?php
namespace Wap\Controller;
use Common\Controller\WapController;

/**
 *在线缴费
 * yaoyongli 2016年1月4日
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class PaymentController extends WapController{
	protected function _initialize() {
		parent::_initialize ();
		$this->manInfo = $this->getInfo('name,phone,pic', 'owner', 'id='.session('fansInfo.typeid'));
		$this->assign('manInfo', $this->manInfo);
		//分享
		 if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
			$this->assign('shareJs', $shareJs);
		} 
	}
	/**
	 * 首页
	 * yaoyongli 2016年1月7日
	 */ 
	public function index(){
		//获取缴费
//		$where = 'b.id = d.bill_id and d.status > 0 and b.status = 3 and d.rid = '.session('fansInfo.rid');
//		$list = $this->getList('d.id,d.porperty,d.energy,d.arrear_money,d.carport,d.car_manger,d.water,d.status,d.total_money,d.porperty_pay,d.energy_pay,d.arrear_money_pay,d.car_manger_pay,d.carport_pay,d.water_pay,b.name', array('payment_detail as d','bill as b'), $where, 'b.start_time desc');


        //获取业主信息
//        $ownerInfo = $this->getInfo('o.id,o.address,o.pic,a.name as area,o.point', 'owner as o, app_area as a', 'a.id = o.aid and o.id='.session('fansInfo.typeid'));
//        $ownerInfo['addr'] = array_reverse(explode('-', $ownerInfo['addr']));
//        $this->assign('ownerInfo', $ownerInfo);
        //获取缴费
        $where = 'b.id = d.bill_id and d.status > 0 and b.status = 3 and d.rid = '.session('fansInfo.rid');
        $list = $this->getList('d.id,d.porperty,d.energy,d.arrear_money,d.carport,d.car_manger,d.water,d.status,d.total_money,d.porperty_pay,d.energy_pay,d.arrear_money_pay,d.car_manger_pay,d.carport_pay,d.water_pay,b.name', array('payment_detail as d','bill as b'), $where, 'b.start_time desc');

        $this->assign('list', $list);
        $this->display();
	}
	/**
	 * 详情
	 * huying Jan 22, 2016
	 */
	public function payDetail(){
		$info = $this->getInfo('id,create_time,real_money,point,status,money,type,typeid', 'payment', 'id='.I('get.id', 0, 'intval'));
		$this->assign('info', $info);
		$this->display();
	}
	/**
	 * 列表
	 * huying Jan 22, 2016
	 */
	public function lists(){
		//获取所有的缴费记录
		$list = $this->getList('id,money,real_money,remark,create_time,status,pay_time', 'payment', ' status=2 and oid='.session('fansInfo.typeid'), 'create_time desc');
		foreach ($list as $k => $v){
			if($v['create_time'] >= strtotime(date('Y-m-01'))){//本月
				$data['this'][] = $v; 
			}else if($v['create_time'] > (strtotime(date('Y').'-'.(date('m')-1).'-01'))){//上月
				$data['last'][] = $v;
			}else{//其余
				$data['other'][] = $v;
			}
		}
		$this->assign('data', $data);
		$this->display();
	}
	
	/**
	 * 积分支付
	 * huying Feb 17, 2016
	 */
	public function point_pay(){
		$point = I('post.point', 0, 'intval');
		$cate = I('post.cate', '');
		if(!$cate){
			$this->ajaxReturn(array('status' => 0, 'info' => '请选择缴费项'));
		}
		$id = I('post.id', 0, 'intval');
		if($id > 0){
			$cate_arr = array_filter(explode(',', $cate));
			foreach($cate_arr as $value) {
				$cate_pay[] = $value . '_pay';
			}
			$info = $this->getInfo('bill_id,status,' . $cate . ',' . implode(',', $cate_pay), 'payment_detail', 'id=' . $id);
			if($info['status'] == 2){
				$this->ajaxReturn(array('status' => 0, 'info' => '该物业费已经有人缴过了'));
			}
			$this->deleteData('status = 1 and type = 1 and typeid=' . $id . ' and oid=' . session('fansInfo.typeid'), 'payment');
			$aid = M('bill')->where('id=' . $info['bill_id'])->getField('aid');
			foreach($cate_arr as $value) {
				$money += $info[$value] - $info[$value . '_pay'];
			}
			$result = $this->updateData(array('oid' => session('fansInfo.typeid'), 'status' => 1, 'aid' => $aid, 'money' => $money, 'create_time' => time(), 'type' => 1, 'typeid' => $id, 'remark' => '在线缴费', 'point' => $point, 'pay_cate' => $cate), 'payment');
			if($result){
				$this->ajaxReturn(array('status' => 2, 'info' => '缴费成功', 'result' => $result));
			}
			$this->returnResult(false, array('操作成功', '缴费失败'));
		}
		$this->returnResult(false, array('操作成功', '缴费失败'));

	}
	public function pay_status(){
		$this->display();
	}
	
}