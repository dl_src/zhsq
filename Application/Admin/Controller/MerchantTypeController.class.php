<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class MerchantTypeController extends AdminController{

	public function index(){
		$where = '1=1';
		$where .= I('get.name', '', 'strval') ? ' and name like "%' . I('get.name', '', 'strval') . '%"' : '';
		// $where .= I('get.status', -1, 'intval') == -1 ? '' : ' and status=' . I('get.status', -1, 'intval');
		if(I('get.status') < 2){
			// $where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and status='.I('get.status') : ' and status < 2';
			// $where .= I('get.status') > -1 && !is_null($_GET['status']) ? ' and o.status ='.I('get.status') : ' and o.status < 2';
			$where .= I('get.status') != '' && I('get.status') > -1 ? ' and status=' . I('get.status') : ' and status < 2';
		}
		$list = $this->getList('id,name,sort,status', 'merchant_type', $where, 'sort asc', true);
		$this->assign('list', $list);
		$this->display();
	}

	public function add(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'merchant_type');
			$this->returnResult($result, null, U('MerchantType/index'));
		}
	}

	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'merchant_type', 2);
			$this->returnResult($result, null, U('MerchantType/index'));
		}else{
			$data = $this->getInfo('id,pic,name,desc', 'merchant_type', 'id=' . I('get.id', 0, 'intval'));
			$this->ajaxReturn($data);
		}
	}

	public function del(){
		$result = M('merchant_type')->where('id=' . I('get.id', 0, 'intval'))->setField('status', 2);
		$this->returnResult($result);
	}
}