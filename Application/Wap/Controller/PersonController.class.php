<?php
namespace Wap\Controller;
use Common\Controller\WapController;

/**
 *个人中心
 * yaoyongli 2016年1月4日
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class PersonController extends WapController{
	protected function _initialize() {
		parent::_initialize ();
		//分享
	 	 if(!IS_AJAX){
			$shareJs = $this->getShareJs(C('share_title'), C('share_pic'), U('Public/index'), C('share_desc'));
 			$this->assign('shareJs', $shareJs);
 		}  
	}
	/**
	 * 首页
	 * yaoyongli 2016年1月7日
	 */
	public function index(){
		$pid = $this->getInfo('o.id,o.pid,o.point,o.address,rom.area,rom.room,o.name,o.pic',array('owner o','owner_room rom'),'o.id = rom.oid and o.id='.session('fansInfo.typeid'));
		$this->assign('pid', $pid);
		$this->display();
	}
	/**
	 * 个人信息
	 * huying Jan 13, 2016
	 */
	public function edit(){		
		$info = $this->getInfo('o.id,o.name,o.phone,o.pic,o.address,o.nickname,ro.area,ro.room',array('owner o','owner_room ro'),'ro.oid = o.id and o.id='.session('fansInfo.typeid'));
		$this->assign('info', $info);
		/*$data = array_merge(array("scope" => 'temp', "deadline" => time() + 3600), array('returnUrl' => C('site_url').U('Wap/Person/upload',array('id'=>session('fansInfo.typeid'))), 'returnBody' => '{"url":"http://7xiy6d.com1.z0.glb.clouddn.com/$(key)"}'));
		$token = $this->token($data, 'XAX8AcIUuCVrjqOx3t6ZF9U3gHaIG9fVvc6xQSl1', 'JZ6bJQR2nt4cjJBONXqaUnBp64DXTExN4iJtyPZk');
		$this->assign('token', $token);*/
		$this->display();
	}
	
/*	private function token($data, $accessKey, $secretKey){
		$data = str_replace(array('+', '/'), array('-', '_'), base64_encode(json_encode($data)));
		$sign = hash_hmac('sha1', $data, $secretKey, true);
		return $accessKey.':'.str_replace(array('+', '/'), array('-', '_'), base64_encode($sign)).':'.$data ;
	}*/
	/**
	 * 上传照片
	 * huying 2015-9-9
	 */
	/*public function upload(){
		$str = json_decode(base64_decode(str_replace(array('-', '_'), array('+', '/'), $_GET['upload_ret'])), true);
		exit('{"error":0, "url": "'.$str['url'].'"}');
	}*/

	/**
	 * 上传头像
	 * huying Mar 7, 2016
	 */
	public function upload(){
		$result = M('owner')->where('id='.session('fansInfo.typeid'))->setField('pic', $_POST['pic'][0]);
		$this->returnResult($result);
	} 
	
	/**
	 * 修改电话
	 * huying Jan 13, 2016
	 */
	public function tel(){
		if(IS_POST){
			if(!$_POST['phone'] || !$_POST['verify'] || !preg_match('/^1[3-8]\d{9}$/', $_POST['phone'])){
				$this->ajaxReturn(array('info' => '请输入正确的信息', 'status' => 0));
			}else{
				if(cookie('verify_code_' . $_POST['phone']) === sha1(date('Ym') . $_POST['phone'] . $_POST['verify'])){
					$oid = M('owner')->where('status=1 and phone=' . $_POST['phone'])->getField('id');
					if($oid > 0){
						$this->ajaxReturn(array('status' => 0, 'info' => '此手机号已被绑定'));
					}
					//修改业主的手机号码
					$result = M('owner')->where('id='.session('fansInfo.oid'))->setField('phone', $_POST['phone']);
					if($result !== false){
						$this->ajaxReturn(array('info' => '修改成功', 'status' => 1));
					}
				}else{
					$this->ajaxReturn(array('info' => '手机验证码错误', 'status' => 0));
				}
			}
			$this->ajaxReturn(array('info' => '修改失败', 'status' => 0));
		}else{
			$this->display();
		}
	}
	/**
	 * 修改地址
	 * huying Jan 13, 2016
	 */
	public function address(){
		if(IS_POST){
			if(!$_POST['address']){
				$this->ajaxReturn(array('info' => '请输入收货地址', 'status' => 0));
			}else{
				$result = M('owner')->where('id='.session('fansInfo.typeid'))->setField('address', $_POST['address']);
				if($result !== false){
					$this->ajaxReturn(array('info' => '修改成功', 'status' => 1));
				}
			}
			$this->ajaxReturn(array('info' => '修改失败', 'status' => 0));
		}else{
			$address = M('owner')->where('id='.session('fansInfo.typeid'))->getField('address');
			$this->assign('address', $address);
			$this->display();
		}
	}
	/**
	 * 修改昵称
	 * huying Jan 13, 2016
	 */
	public function nickname(){
		if(IS_POST){
			if(!$_POST['nickname']){
				$this->ajaxReturn(array('info' => '请输入昵称', 'status' => 0));
			}else{
				$resul= M('owner')->where('id='.session('fansInfo.typeid'))->setField('nickname', $_POST['nickname']);
				$this->returnResult($result);
			}
		}else{
			$nickname = M('owner')->where('id='.session('fansInfo.typeid'))->getField('nickname');
			$this->assign('nickname', $nickname);
			$this->display();
		}
	}
	/**
	 * 更换头像
	 * huying Jan 13, 2016
	 */
	public function head(){
	if(IS_POST){
			if(!$_POST['pic']){
				$this->ajaxReturn(array('info' => '请输入上传图片', 'status' => 0));
			}else{
				$result = M('owner')->where('id='.session('fansInfo.typeid'))->setField('pic', $_POST['pic']);
				if($result !== false){
					session('fansInfo.pic', $_POST['pic']);
					$this->ajaxReturn(array('info' => '修改成功', 'status' => 1));
				}
			}
			$this->ajaxReturn(array('info' => '修改失败', 'status' => 0));
		}else{
			$pic = M('owner')->where('id='.session('fansInfo.typeid'))->getField('pic');
			$this->assign('pic', $pic);
			$this->display();
		} 
	}


	/**
	 *
	 * 修改头像
	 * dingchao 2015-4-25
	 */
	public function alterAvatar(){
		$pic=$_POST['pic'];
		$field =  array('pic'=>$pic, 'id' => session('fansInfo.typeid'));
		$result = $this->updateData($field, 'owner', 2);
		//$result=M('member')->field('logo')->where('id='.session('mid'))->save();
		$e=M()->_sql();
		if($result){
			echo '修改成功';
		}else{
			echo '修改失败';
		}
	}
	/**
	 * 上传图片
	 * @return string 上传信息
	 * dingchao 2015-5-4
	 */
	// public function upload(){
	//      $path = '/zhsq/Runtime/Temp/';
	// 	if (!file_exists($_SERVER["DOCUMENT_ROOT"].$path)){
	// 		mkdir($_SERVER["DOCUMENT_ROOT"].$path, 0777, true);
	// 		chmod($_SERVER["DOCUMENT_ROOT"].$path, 0777);
	// 	}
	// 	$fileName = $path.date('YmdHis').strrchr($_FILES['file']['name'], '.');
	// 	move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER["DOCUMENT_ROOT"].$fileName);
	// 	exit('{"error":0, "url": "'.$fileName.'"}'); 
	// 	$str = json_decode(base64_decode(str_replace(array('-', '_'), array('+', '/'), $_GET['upload_ret'])), true);
	// 	exit('{"error":0, "url": "'.$str['url'].'"}');
	// }

	/**
	 * 技术支持
	 * huying Jan 13, 2016
	 */
	public function tech(){
		$this->display();
	}
}