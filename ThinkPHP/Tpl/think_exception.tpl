<?php
    if(C('LAYOUT_ON')) {
        echo '{__NOLAYOUT__}';
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>404</title>
</head>
<body>
<p style="text-align:center;"><?php echo strip_tags($e['message']);?></p>
<?php if(isset($e['file'])) {?>
<p>FILE: <?php echo $e['file'] ;?> &#12288;LINE: <?php echo $e['line'];?></p>
<?php }?>
<?php if(isset($e['trace'])) {?>
<p><?php echo nl2br($e['trace']);?></p>
<?php }?>
</body>
</html>