<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

class ForumPlateController extends AdminController{

	public function index(){
		$list = $this->getList('id,name,sort,status', 'forum_plate', '1 = 1', 'id desc', true);
		$this->assign('list', $list);
		$this->display();
	}

	public function add(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'forum_plate');
			$this->returnResult($result, null, U('ForumPlate/index'));
		}
	}

	/**
	 * 获取分类的详细信息
	 */
	public function edit(){
		if(IS_POST){
			$result = $this->updateData($_POST, 'forum_plate', 2);
			$this->returnResult($result, null, U('ForumPlate/index?p=' . $_POST['p']));
		}else{
			$data = $this->getInfo('id,name,desc', 'forum_plate', 'id=' . I('get.id', 0, 'intval'));
			$this->ajaxReturn($data);
		}
	}

	public function del(){
		$id = I('get.id', 0, 'intval');
		$result = M('forum_plate')->where('id=' . $id)->setField('status', 2);
		// $result = $this->deleteData('id=' . I('get.id', 0, 'intval'), 'forum_plate');
		$this->returnResult($result);
	}
}