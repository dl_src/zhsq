<?php
namespace Admin\Controller;
use Common\Controller\AdminController;
use Common;

/**
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class ForumController extends AdminController{

	/**
	 * 帖子列表显示
	 * 
	 */
	public function index(){
		$where = 'f.cate_id = p.id';
		$where .= I('get.cate_id') > 0 ? ' and f.cate_id=' . I('get.cate_id', 0, 'intval') : '';
		$where .= I('get.title') != '' ? ' and f.title like "%' . I('get.title') . '%"' : '';
		$where .= I('get.status', -1) > -1 ? ' and f.status =' . I('get.status') : ' and f.status = 1';
		$list = $this->getList('f.id,f.oid,f.title,f.times,f.status,f.sort,f.posts,f.cate_id', array('forum  f', 'forum_plate p'), $where, 'f.times desc', true);
		$this->assign('list', $list);
		$cateList = $this->getList('id,name', 'forum_plate', 'status = 1', 'id desc');
		$this->assign('cateList', $cateList);
		$this->display();
	}
	
	/**
	 * 发帖
	 * 
	 */
	public function add(){
		if(IS_POST){
			$_POST['fid'] = 0;
			$_POST['times'] = time();
			$_POST['pics'] = implode(',', $_POST['pic']);
			$result = $this->updateData($_POST, 'forum');
			$this->returnResult($result);
		}else{
			$list = $this->getList('id,name', 'forum_plate', 'status = 1', 'sort desc');
			$this->assign('plateList', $list);
			$this->display();
		}
	}
	
	/**
	 * 修改
	 * huying Mar 8, 2016
	 */
	public function edit(){
		if(IS_POST){
			$_POST['pics'] = implode(',', $_POST['pic']);
			$result = $this->updateData($_POST, 'forum', 2);
			$this->returnResult($result);
		}else{
			$list = $this->getList('id,name', 'forum_plate', 'status = 1', 'sort desc');
			$this->assign('plateList', $list);
			$info = $this->getInfo('id,title,desc,pics,cate_id', 'forum', 'id=' . I('get.id', 0, 'intval'));
			if(!empty($info['pics'])){
				$info['pics'] = explode(',', $info['pics']);
			}
			$this->assign('info', $info);
			$this->display('add');
		}
	}
	

	/**
	 * 删除
	 * huying Dec 26, 2015
	 */
	public function del(){
		$result = M('forum')->where('id=' . I('get.id', 0, 'intval'))->setField('status', 2);
		if($result){
			$info = $this->getInfo('id,oid', 'forum', 'id=' . I('get.id', 0, 'intval'));
			$pointInfo = $this->getInfo('p.id,p.point',array('point p','owner o'), 'p.oid = o.id and p.oid = ' . $info['oid']);
			/* if(!empty($pointInfo)){
				$this->changePoint($info['fid'], $pointInfo['point'], '管理员删除帖子', 9, I('get.id', 0, 'intval'), 0);
			} */
		}
		$this->returnResult($result);
	}

	/**
	 * 帖子详情
	 * huying Jan 14, 2016
	 */
	public function detail(){
		$info = $this->getInfo('f.id,f.title,f.desc,f.pics,f.times,o.name,o.pic,o.phone,p.id as pid,p.name as plate,ro.area,ro.room as addr', array('forum as f', 'forum_plate as p', 'owner as o','owner_room ro'), 'ro.oid = o.id and o.id = f.oid and f.cate_id = p.id and f.id = ' . I('post.id', 0, 'intval'));
		if(!empty($info['pics'])){
			$info['pics'] = explode(',', $info['pics']);
		}
		$this->assign('info', $info);
		$postList = $this->getList('p.id,p.desc,p.times,o.name,o.phone,r.name as addr,a.name aname', array('forum_post p', 'owner as o', 'area a','owner_room ro','room r'), 'ro.rid = r.id and ro.oid = o.id and a.id=ro.aid and p.status = 1 and o.id = p.oid and tid=' . I('post.id', 0, 'intval'), 'times asc');
		$this->assign('postList', $postList);
		$this->display();
	}

	/**
	 * 删除回复
	 * huying Dec 26, 2015
	 */
	public function delPost(){
		$result = M('forum_post')->where('id=' . I('get.id', 0, 'intval'))->setField('status', 0);
		if($result){
			$info = $this->getInfo('id,tid,oid', 'forum_post', 'id=' . I('get.id', 0, 'intval'));
			$pointInfo = $this->getInfo('id,point', 'point', 'oid = ' . $info['oid']);
			if(!empty($pointInfo)){
				//$this->changePoint($info['fid'], $pointInfo['point'], '管理员删除回复', 9, I('get.id', 0, 'intval'), 0);
				\Common\Api\CommonApi::ownerPointAct($info['oid'], $point,'管理员删除回复',2);
			}
			M('forum')->where('id=' . $info['tid'])->setDec('posts');
		}
		$this->returnResult($result);
	}

	
	/**
	 * 发表回复
	 * huying Mar 8, 2016
	 */
	public function post(){
		$_POST['oid'] = 0;
		$_POST['times'] = time();
		$result = $this->updateData($_POST, 'forum_post');
		if($result > 0){
			M('forum')->where('id=' . I('post.tid', 0, 'intval'))->setInc('posts');
		}
		$this->returnResult($result);
	}

	/**
	 * 置顶热帖
	 * huying Mar 8, 2016
	 */
	public function setStatus(){
		$status = I('get.status', 0, 'intval');
		$result = M('forum')->where('id=' . I('get.id', 0, 'intval'))->setField('status', $status);
		$this->returnResult($result);
		/*
		 * $result = M('forum')->where('id=' . I('get.id', 0, 'intval'))->setField($field, $status);
		 * $this->returnResult($result);
		 */
	}
}