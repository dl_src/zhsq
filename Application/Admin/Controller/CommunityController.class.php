<?php
namespace Admin\Controller;
use Common\Controller\AdminController;

/**
 * 社区活动管理
 * huying Dec 23, 2015
 * 版权所有：安徽鼎龙网络传媒有限公司
 */
class CommunityController extends AdminController{

	public function index(){
		$where = 'aid in(0,' . session('adminInfo.aids') . ')';
		
		if(I('get.aid', -1, 'intval') != -1){
			if(I('get.aid', -1, 'intval') == 0){
				$where .= ' and  aid =0';
			}else{
				$where .= ' and aid=' . I('get.aid', 0, 'intval');
			}
		}
		$where .= I('get.cate') > 0 ? ' and cate=' . I('get.cate', 0, 'intval') : '';
		$where .= I('get.title') != '' ? ' and title like "%' . I('get.title') . '%"' : '';
		if(I('get.status') < 2){
			$where .= I('get.status') != '' && I('get.status') > -1 ? ' and status=' . I('get.status') : ' and status < 2';
		}
		
		$list = $this->getList('id,title,status,sort,limit_type,aid,sign_num', 'community', $where, 'sort desc', true);
		foreach($list as $k => $v){
			$list[$k]['name'] = $v['aid'] > 0 ? M('area')->where('id=' . $v['aid'])->getField('name') : '全部小区';
		}
		$this->assign('list', $list);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	}

	/**
	 * 添加
	 * huying Dec 26, 2015
	 */
	public function add(){
		if(IS_POST){
			$_POST['start_time'] = strtotime($_POST['start_time']);
			$_POST['end_time'] = strtotime($_POST['end_time']);
			$result = $this->updateData($_POST, 'community');
			$this->returnResult($result, null);
		}else{
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display();
		}
	}

	/**
	 * 修改
	 * huying Dec 26, 2015
	 */
	public function edit(){
		if(IS_POST){
			$_POST['start_time'] = strtotime($_POST['start_time']);
			$_POST['end_time'] = strtotime($_POST['end_time']);
			$result = $this->updateData($_POST, 'community', 2);
			$this->returnResult($result, null);
		}else{
			$info = $this->getInfo('id,title,aid,desc,address,times,pic,limit_type,times,num,start_time,end_time,sort,status', 'community', 'id=' . I('get.id', 0, 'intval'));
			$this->assign('info', $info);
			$areaList = $this->getAreaList();
			$this->assign('areaList', $areaList);
			$this->display('add');
		}
	}

	/**
	 * 删除
	 * huying Dec 26, 2015
	 */
	public function del(){
		$id = I('get.id', 0, 'intval');
		$result = M('community')->where('id=' . $id)->setField('status', 2);
		// $result = $this->deleteData('id=' . I('get.id', 0, 'intval'), 'community');
		$this->returnResult($result);
	}

/**
 * 报名数据
 * huying Dec 26, 2015
 */
	 public function sign(){
// 	 	var_dump($_GET);exit;
	 	$where = 'c.cid = ' .I('get.cid',0,'intval') . ' and c.status = 1 and c.oid = rom.oid';
	 	$where .= I('get.aid') > 0 ? ' and c.aid=' . I('get.aid', 'intval') : '';
	 	$where .= I('get.name') != '' ? ' and c.name like "%' . I('get.name') . '%"' : '';
	 	$where .= I('get.phone') != '' ? ' and c.phone =' . I('get.phone') : '';
	 	$list = $this->getList('c.id,c.cid,c.name,c.phone,c.times,rom.area,rom.room,c.remark,c.oid',array('community_sign as c','owner_room as rom'), $where , 'times desc', true);
// 	    $where =  'c.cid=' . I('get.id', 0, 'intval') . ' and c.status = 1 and c.aid = a.id and ro.oid = o.id and c.oid = o.id';
// 		$where .= I('get.areaid') > 0 ? ' and a.id=' . I('get.areaid', 'intval') : '';
// 		$where .= I('get.name') != '' ? ' and c.name like "%' . I('get.name') . '%"' : '';
// 		$where .= I('get.phone') != '' ? ' and c.phone =' . I('get.phone') : '';
// 		$list = $this->getList('c.id,c.cid,c.name,c.phone,c.times,c.status,a.name as area',array('community_sign c','area a','owner o','owner_room ro'), $where, 'times desc', true);
		$this->assign('list', $list);
		$areaList = $this->getAreaList();
		$this->assign('areaList', $areaList);
		$this->display();
	  }
	  
	  /*
	   * 添加数据
	   * 
	   */
	  public function addSign(){
	  	$infos = $this->getInfo('phone', 'community_sign', 'cid='.$_POST['cid'].' and phone ='.$_POST['phone']);
	  	if(!$infos){
	  	$_POST['times'] = time();
	  	$result = $this->updateData($_POST, 'community_sign');
	  	M('community')->where(array('id'=> $_POST['cid']))->setInc('sign_num');
	  	$this->returnResult($result,null,U('Community/sign',array('cid' => $_POST['cid'])));
	  	}else{
	  		$this->returnResult(null,'已经过报名',U('Community/sign',array('cid' => $_POST['cid'])));
	  	}
	  }
	  
	  /*
	   * 编辑数据信息
	   * 
	   */
	  Public function editSign(){
	  	if(IS_POST){
	  		$result = $this->updateData($_POST, 'community_sign',2);
	  		$this->returnResult($result,null,U('Community/sign',array('cid' => $_POST['cid'])));
	  	}else{
	  		$info = $this->getInfo('id,name,phone,cid,remark,oid','coomunity_sign',array('id' => I('get.id', 0, 'intval')));
	  		if(is_numeric($info['oid']) && $info['oid'] > 0){
	  			$info['oid'] = M('owner_room')->where(array('oid'=> $info['oid']))->getFeild('room');
	  		}
	  		$this->ajaxReturn($info);
	  	}
	  }
	  
	 

/**
 * 删除
 * huying Dec 26, 2015
 */
	/*
	 * public function delSign(){
	 * $result = $this->deleteData('id=' . I('get.id', 0, 'intval'), 'community_sign');
	 * $this->returnResult($result);
	 * }
	 */

/**
 * 导出数据
 * huying Jan 28, 2016
 */
	 public function export(){
	 	$where = 'c.cid = ' .I('get.cid',0,'intval') . ' and c.status = 1 and c.oid = rom.oid';
	 	$where .= I('get.aid') > 0 ? ' and c.aid=' . I('get.aid', 'intval') : '';
	 	$where .= I('get.name') != '' ? ' and c.name like "%' . I('get.name') . '%"' : '';
	 	$where .= I('get.phone') != '' ? ' and c.phone =' . I('get.phone') : '';
	 	$list = $this->getList('c.name,c.phone,date_format(from_unixtime(c.times),"%Y-%m-%d %H:%i:%s") as times,rom.area',array('community_sign as c','owner_room as rom'), $where , 'times desc', true);
		if($list){
			$title = array('姓名', '手机', '报名时间', '所属小区');
			/* $arr = array_merge(array(array(0 => $name )), $title, $data);
			$export = \Common\Api\PHPExcelApi::exportExcel($arr, $name);
			echo iconv('gb2312', 'utf-8', $export); */
			array_unshift($list, $title);
			 \Common\Api\PHPExcelApi::exportExcel($list, 'Community_' . date('Ymd'));
			//$this->ajaxReturn(array('info' => '导出成功', 'status' => 1, 'url' => $file));
		}else{
			$this->error('没有数据');
		}
	 }
	 
}